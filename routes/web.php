<?php
use App\Http\Controllers\Profilecontroller;
use App\Http\Controllers\Cuticontroller;

Route::get('/', function () {
    if (!Auth::user()) {
        return redirect()->to('/login');
    }else{
        return redirect()->to('/halaman-utama');
    }
});
Route::resource('/halaman-utama','PresensiController');
Route::resource('/halaman-utama','PresensiController');
Route::get('/profile',[Profilecontroller::class,'index'])->name('profile.index');

Route::get('/cuti',[Cuticontroller::class,'index'])->name('cuti.index');
// Auth::routes();
Auth::routes(['register' => false]);












































/* Auth Manual disini 
----------------------
use App\Http\Controllers\Auth\UsermobileLoginController;
use App\Http\Controllers\UsermobileRegisterController;

Route::get('/create', [UsermobileRegisterController::class, 'index'])->name('index');

Route::get('/user-login', [UsermobileLoginController::class, 'showLoginForm'])->name('karyawan.login');
Route::post('/user-login', [UsermobileLoginController::class, 'login']);

Route::middleware(['web', 'usermobile'])->group(function () {
    // Route yang memerlukan autentikasi usermobile
});
Route::post('/', [UsermobileLoginController::class, 'login']);
Route::get('/index', function () {
    return view('pages_mobile.presensi.index');
});


Route::get('/login', function () {
    return view('auth_manual/login');
});

Route::get('/register', function () {
    return view('auth_manual/register');
});
Route::get('/home', 'HomeController@index')->name('home');
*/


