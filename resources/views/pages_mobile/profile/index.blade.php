<!DOCTYPE html>
<html lang="zxx">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
	<link rel="icon" href="images/favicon.png">
	<title>Maxui</title>

	<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900&display=swap" rel="stylesheet">

	{{-- <link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/ionicons.min.css">
	<link rel="stylesheet" href="css/fakeLoader.css">
	<link rel="stylesheet" href="css/swiper.min.css">
	<link rel="stylesheet" href="css/style.css"> --}}

    @include('layout_mobile.css')


</head>
<body>
	
	<!-- fakeloader -->
	<div class="fakeLoader"></div>
	<!-- end fakeloader -->
	
	<!-- navbar -->
	<div class="navbar">
		<div class="left">
			<a href="{{URL::to('/halaman-utama')}}" class="link link-back"><i class="icon ion-ios-arrow-back"></i></a>
		</div>
		<div class="title">
			Profile
		</div>
		<div class="right">
			
		</div>
	</div>
	<!-- end navbar -->

	<!-- pages wrapper -->
	<div class="pages-wrapper">

		<!-- separator -->
		<div class="separator-large"></div>
		<!-- end separator -->
		
		<!-- profile -->
		<div class="profile">
			<div class="container">
				<div class="row">
					<div class="col-4 align-self-center">
						<div class="content statistic text-right">
							{{-- <h5>500</h5>
							<span class="text-small">Profile Posts</span> --}}
						</div>
					</div>
					<div class="col-4">
						<div class="header-profile">
							<div class="container">
								<img src="{{asset('mobile_theme/images/no-photo.png')}}" alt="image-demo">
							</div>
						</div>
					</div>
					<div class="col-4 align-self-center">
						<div class="content statistic text-left">
							{{-- <h5>48K</h5>
							<span class="text-small">Followers</span> --}}
						</div>
					</div>
				</div>

				<div class="profile-title text-center">
					<h4>{{$employee->employee_name}}</h4>
					<span class="text-small">{{$employee->get_emp_work_units->work_unit_desc}}</span>
				</div>

				<div class="profile-button">
					<div class="row">
						<div class="col-12">
							<a href="#" class="button btn-default" style="background-color:#fe2775" >DATA DIRI</a>
						</div>
						{{-- <div class="col-6">
							<a href="#" class="button button-outline">Message</a>
						</div> --}}
					</div>
				</div>

				<!-- separator -->
				<div class="separator-large"></div>
				<!-- end separator -->


				
				<!-- separator -->
				{{-- <div class="separator-large"></div> --}}
				<!-- end separator -->

				{{-- <div class="section-title"> --}}
					{{-- <h6>" Hidup adalah Perjuangan "</h6>
					<span class="section-subtitle">Jangan Menyerah <code>print_f("!= giveup")</code></span> --}}
				{{-- </div> --}}

				<div class="list-view list-separate-two">
					<ul>
						<li class="list-item">
							<div class="list-media">
								
								<i class="icon ion-ios-contact"></i>
							</div>
							<div class="list-label">
								<div class="list-title">{{$employee->employee_name}}</div>
								{{-- <div class="list-after"><i class="icon ion-ios-arrow-forward"></i></div> --}}
							</div>
						</li>
						<li class="list-item">
							<div class="list-media">
								<i class="icon ion-ios-list-box"></i>
							</div>
							<div class="list-label">
								<div class="list-title">{{$employee->employee_number}}</div>
								{{-- <div class="list-after"><i class="icon ion-ios-arrow-forward"></i></div> --}}
							</div>
						</li>
						<li class="list-item">
							<div class="list-media">
								<i class="icon ion-ios-alert"></i>
							</div>
							<div class="list-label">
								<div class="list-title">{{$datacutiterakhir->cuti}} Hari Kuota Cuti Tahunan</div>
								{{-- <div class="list-after"><i class="icon ion-ios-arrow-forward"></i></div> --}}
							</div>
						</li>
					</ul>
				</div>
				

			</div>
		</div>
		<!-- end profile -->

	</div>
	<!-- end pages wrapper -->

    @include('layout_mobile.js')


	{{-- <script src="js/jquery-3.4.1.min.js"></script>
	<script src="js/bootstrap.bundle.min.js"></script>
	<script src="js/fakeLoader.js"></script>
	<script src="js/swiper.min.js"></script>
	<script src="js/main.js"></script> --}}

</body>
</html>