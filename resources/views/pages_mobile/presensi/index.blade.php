<!DOCTYPE html>
<html lang="zxx">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
      <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
      <link rel="icon" href="images/favicon.png">
      <title>App Presensi</title>
      <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900&display=swap" rel="stylesheet">
      {{-- 
      <link rel="stylesheet" href="https://unpkg.com/leaflet@1.9.4/dist/leaflet.css"
         integrity="sha256-p4NxAoJBhIIN+hmNHrzRCf9tD/miZyoHS5obTRR9BMY="
         crossorigin=""/>
      <script src="https://unpkg.com/leaflet@1.9.4/dist/leaflet.js"
         integrity="sha256-20nQCchB9co0qIjJZRGuk2/Z9VM+kNiyxNV1lvTlZBo="
         crossorigin=""></script> --}}
      @include('layout_mobile.css')
      <script src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.26/webcam.min.js" integrity="sha512-dQIiHSl2hr3NWKKLycPndtpbh5iaHLo6MwrXm7F0FM5e+kL2U16oE9uIwPHUl6fQBeCthiEuV/rzP3MiAB8Vfw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
      {{-- webcam js  --}}
      {{-- end webcam js --}}
   </head>
   <style type="text/css">
      .pushable {
      background: hsl(340deg 100% 32%);
      border-radius: 12px;
      border: none;
      padding: 0;
      cursor: pointer;
      outline-offset: 4px;
      }
      .front {
      display: block;
      padding: 12px 42px;
      border-radius: 12px;
      font-size: 1.25rem;
      background: hsl(345deg 100% 47%);
      color: white;
      transform: translateY(-6px);
      }
      .pushable:active .front {
      transform: translateY(-2px);
      }
      #camera_id{
      /* padding:20px; border:1px solid; background:#ccc;  */
      }
      #btn_presensi{
      display: none;
      }
	  #infocounter{
	  display: none;
	  }
      /* #dummyimg{
      display:none;
      } */
   </style>
   <body onload="startTime()">
      <!-- fakeloader -->
      <div class="fakeLoader"></div>
      <!-- end fakeloader -->
      <div class="navbar">
         <div class="left">
            <a href="{{URL::to('/halaman-utama')}}" class="link link-back"><i class="icon ion-ios-arrow-back"></i></a>
         </div>
         <div class="title">
            Halaman Presensi
         </div>
         <div class="right">
         </div>
      </div>
      {{-- modal  --}}
      <!-- Modal -->
      <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Camera Picture</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
               </div>
               <div class="modal-body">
                  {{-- 
                  <div id="camera_id"></div>
                  --}}
                  {{-- card --}}
                  <div class="card">
                     <div class="card-body">
                        <span id="camera_id"></span>
                     </div>
                  </div>
                  {{-- card --}}
                  {{-- <input type="hidden" name="image" class="image-tag"> --}}
               </div>
               <div class="modal-footer">
                  <button type="button" class="btn btn-default" onClick="configure()" id="btn_config" style="background-color:#0b7845;color:white">Nyalakan Kamera</button>
                  <button type="button" class="btn btn-default" onClick="take_snapshot()" id="btn_capture" style="background-color:#780B3E;color:white">Capture</button>
               </div>
            </div>
         </div>
      </div>
      {{-- end modal --}}
      <!-- pages wrapper -->
      <div class="pages-wrapper">
         <!-- separator -->
         <div class="separator-large"></div>
         <!-- end separator -->
         <!-- grid system -->
         <div class="grid-system">
            <div class="container">
               <div class="row">
                  <div class="col-6">
                     {{-- tanggal --}}
                     <center>
                        {{-- 
                        <p></p>
                        --}}
                        <h6><span class="badge badge-default">{!! substr(\Carbon\carbon::now(),0,10)!!}</span></h6>
                     </center>
                  </div>
                  <div class="col-6">
                     {{-- timer --}}
                     <center>
                        <h6>
                           <span class="badge badge-default">
                              <div id="txt"></div>
                           </span>
                        </h6>
                     </center>
                  </div>
               </div>
               <!-- separator -->
               <div class="separator-large"></div>
               <!-- end separator -->
            </div>



            {{-- form photo --}}
            <div class="row">
               <div class="col-12">
                  {{-- open  --}}
                  <form id="myform" action="{{route('halaman-utama.store')}}" method="post">
                     <input id="mydata" type="hidden" name="mydata" value=""/>
                     <div class="container">
                        <div class="card card-image card-outline" id="dummyimg">
                           <center>
                              <svg fill="#b5b5b5" height="200px" width="200px" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 487 487" xml:space="preserve" stroke="#b5b5b5">
                                 <g id="SVGRepo_bgCarrier" stroke-width="0"></g>
                                 <g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g>
                                 <g id="SVGRepo_iconCarrier">
                                    <g>
                                       <g>
                                          <path d="M308.1,277.95c0,35.7-28.9,64.6-64.6,64.6s-64.6-28.9-64.6-64.6s28.9-64.6,64.6-64.6S308.1,242.25,308.1,277.95z M440.3,116.05c25.8,0,46.7,20.9,46.7,46.7v122.4v103.8c0,27.5-22.3,49.8-49.8,49.8H49.8c-27.5,0-49.8-22.3-49.8-49.8v-103.9 v-122.3l0,0c0-25.8,20.9-46.7,46.7-46.7h93.4l4.4-18.6c6.7-28.8,32.4-49.2,62-49.2h74.1c29.6,0,55.3,20.4,62,49.2l4.3,18.6H440.3z M97.4,183.45c0-12.9-10.5-23.4-23.4-23.4c-13,0-23.5,10.5-23.5,23.4s10.5,23.4,23.4,23.4C86.9,206.95,97.4,196.45,97.4,183.45z M358.7,277.95c0-63.6-51.6-115.2-115.2-115.2s-115.2,51.6-115.2,115.2s51.6,115.2,115.2,115.2S358.7,341.55,358.7,277.95z"></path>
                                       </g>
                                    </g>
                                 </g>
                              </svg>
                              {{-- 
                              <svg fill="#fe2775" height="200px" width="200px" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 487 487" xml:space="preserve" stroke="#fe2775">
                                 <g id="SVGRepo_bgCarrier" stroke-width="0"></g>
                                 <g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g>
                                 <g id="SVGRepo_iconCarrier">
                                    <g>
                                       <g>
                                          <path d="M308.1,277.95c0,35.7-28.9,64.6-64.6,64.6s-64.6-28.9-64.6-64.6s28.9-64.6,64.6-64.6S308.1,242.25,308.1,277.95z M440.3,116.05c25.8,0,46.7,20.9,46.7,46.7v122.4v103.8c0,27.5-22.3,49.8-49.8,49.8H49.8c-27.5,0-49.8-22.3-49.8-49.8v-103.9 v-122.3l0,0c0-25.8,20.9-46.7,46.7-46.7h93.4l4.4-18.6c6.7-28.8,32.4-49.2,62-49.2h74.1c29.6,0,55.3,20.4,62,49.2l4.3,18.6H440.3z M97.4,183.45c0-12.9-10.5-23.4-23.4-23.4c-13,0-23.5,10.5-23.5,23.4s10.5,23.4,23.4,23.4C86.9,206.95,97.4,196.45,97.4,183.45z M358.7,277.95c0-63.6-51.6-115.2-115.2-115.2s-115.2,51.6-115.2,115.2s51.6,115.2,115.2,115.2S358.7,341.55,358.7,277.95z"></path>
                                       </g>
                                    </g>
                                 </g>
                              </svg>
                           </center>
                           --}}
                           {{-- <input type="hidden" name="image" id="imageprev">
                           <input type="hidden" name="image" class="image-tag"> --}}
                           {{-- 
                           <div class="card-body">
                              <h5 class="card-title">John Doe</h5>
                              <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus, veniam.</p>
                              <div class="card-date"><i class="icon ion-ios-calendar"></i> Feb 12</div>
                           </div>
                           --}}
                        </div>
                        {{-- 
                        <a href="{{URL::to('/photo')}}">
                           --}}
                           <div class="card card-image ">
                              <center>
                                 {{-- <img src="{{asset('mobile_theme/images/horizontal5.jpg')}}" class="card-img" alt="image-demo"> --}}
                                 {{-- 
                                 <svg fill="#fe2775" height="200px" width="200px" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 487 487" xml:space="preserve" stroke="#fe2775">
                                    <g id="SVGRepo_bgCarrier" stroke-width="0"></g>
                                    <g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g>
                                    <g id="SVGRepo_iconCarrier">
                                       <g>
                                          <g>
                                             <path d="M308.1,277.95c0,35.7-28.9,64.6-64.6,64.6s-64.6-28.9-64.6-64.6s28.9-64.6,64.6-64.6S308.1,242.25,308.1,277.95z M440.3,116.05c25.8,0,46.7,20.9,46.7,46.7v122.4v103.8c0,27.5-22.3,49.8-49.8,49.8H49.8c-27.5,0-49.8-22.3-49.8-49.8v-103.9 v-122.3l0,0c0-25.8,20.9-46.7,46.7-46.7h93.4l4.4-18.6c6.7-28.8,32.4-49.2,62-49.2h74.1c29.6,0,55.3,20.4,62,49.2l4.3,18.6H440.3z M97.4,183.45c0-12.9-10.5-23.4-23.4-23.4c-13,0-23.5,10.5-23.5,23.4s10.5,23.4,23.4,23.4C86.9,206.95,97.4,196.45,97.4,183.45z M358.7,277.95c0-63.6-51.6-115.2-115.2-115.2s-115.2,51.6-115.2,115.2s51.6,115.2,115.2,115.2S358.7,341.55,358.7,277.95z"></path>
                                          </g>
                                       </g>
                                    </g>
                                 </svg>
                              </center>
                              --}}
                              <div id="imageprev"></div>
                              <input type="hidden" name="image" id="imageprev">
                              <input type="hidden" name="image" class="image-tag">
                              {{-- 
                              <div class="card-body">
                                 <h5 class="card-title">John Doe</h5>
                                 <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus, veniam.</p>
                                 <div class="card-date"><i class="icon ion-ios-calendar"></i> Feb 12</div>
                              </div>
                              --}}
                           </div>
                        </a>
                     </div>
                     {{-- close --}}
               </div>
            </div>
            {{-- end  --}}
            <div class="row">
            <div class="col-12">
            @csrf
            <center>
            <button class="pushable" type="submit" id="btn_presensi">
            <span class="front">
            Presensi
            </span>
            </button>
            </center>
            </form>
            {{-- <button class="pushable" type="submit" onclick="saveSnap()">
            <span class="front">
            Save Gambar
            </span>
            </button> --}}
            <center>
            <button class="pushable" type="submit" id="btn_modal_presensi" data-toggle="modal" data-target="#exampleModal">
            <span class="front">
            Presensi
            </span>
            </button>
            </center>
            {{-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
            Launch demo modal
            </button> --}}
            </div>			
            </div>
            {{-- separator --}}
            <div class="separator-large"></div>

			{{-- <div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-body">
							<h5 style="text-align: center">counter</h5>
						</div>
					</div>
				</div>	
			</div> --}}
            <div class="row">
               <div class="col-12">
				 <div  id="infocounter">
                  <div class="card bg-default">
                     <div class="card-body">
                        <p class="card-text"> Batas maksimal Presensi sebanyak 4 kali dalam satu hari. Jika kuota Presensi Anda telah terpenuhi, harap lakukan presensi menggunakan mesin absensi yang tersedia di lobi. Sisa kuota Presensi Anda saat ini:  </p>
                     </div>
                  </div>
				</div>
               </div>
            </div>
            {{-- untuk loping gambar --}}
            <div class="container">
               <div class="row">
                  <div class="col-12">
                     {{-- @isset($historyPresensi) --}}
                     @foreach ($historyPresensi as $hp)
                     <div class="card card-image card-outline" style="margin-top:5px">
                        {{-- <img src="images/horizontal5.jpg" class="card-img" alt="image-demo"> --}}
                        <div class="card-body">
                           <h6 class="card-title">{!!substr($hp->created_at,0,16)!!}</h6>
                           <img src="data:image/png;base64,{{ chunk_split(base64_encode($hp->gambar_pegawai)) }}" height="200" width="100">
                           {{-- 
                           <div class="card-date"><i class="icon ion-ios-calendar"></i> Feb 12</div>
                           --}}
                        </div>
                     </div>
                     {{-- $binary_data = base64_decode( $encoded_data ); --}}
                     {{-- card --}}
                     {{-- endcard --}}
                     @endforeach
                     {{-- @endisset --}}
                  </div>
               </div>
            </div>
            {{-- end looping gambar --}}
         </div>
      </div>
      <!-- end grid system -->
      </div>
      <!-- end pages wrapper -->
      <!-- toolbar bottom -->
      @include('layout_mobile.menubawa')
      <!-- end toolbar bottom -->
      @include('layout_mobile.js')
      {{-- js --}}
      {{-- end js --}}
      <script>
         $(document).ready(function(){
         	// alert("test");
         
         	$('#btn_presensi').hide();
         	$('#btn_capture').attr('disabled','disabled');
         });
         
         
         function configure(){
         	Webcam.set({
         		width: 320,
         		height: 240,
         		image_format: 'jpeg',
         		jpeg_quality: 90
         	});
         	Webcam.attach('#camera_id' );
         	$('#btn_capture').attr('disabled','disabled');
         	$('#btn_capture').removeAttr('disabled');
         }
         
         
         function take_snapshot() {
         	Webcam.snap( function(data_uri) {
         		// display results in page
         		var data = $("#imageprev").val(data_uri);
         		var raw_image_data = data_uri.replace(/^data\:image\/\w+\;base64\,/, '');
         		document.getElementById('mydata').value = raw_image_data;
         		// document.getElementById('myform').submit(); 
         		document.getElementById('imageprev').innerHTML = '<img id="result" src="'+data_uri+'"/>';
         	} );
         	$('#exampleModal').modal("hide");
         	// Webcam.reset();
         	//show button modal presensi
         	$('#btn_presensi').show();
         	$('#btn_modal_presensi').hide(); //modal hide
         	$('#dummyimg').css("display", "none");
           }
         
         function kirim_absensi(){
         document.getElementById('myform').submit(); 
         }
         function saveSnap(){
         	// Get base64 value from <img id='imageprev'> source
         	var base64image =  document.getElementById("result").src;
         	console.log(base64image);
         	 Webcam.upload( base64image, 'upload.php', function(code, text) {
         	console.log('Save successfully');
                   });
         
         }
         
         // end camera js
         function startTime() {
           const today = new Date();
           let h = today.getHours();
           let m = today.getMinutes();
           let s = today.getSeconds();
           m = checkTime(m);
           s = checkTime(s);
           document.getElementById('txt').innerHTML =  h + ":" + m + ":" + s;
           setTimeout(startTime, 1000);
         }
         
         function checkTime(i) {
           if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
           return i;
         }
      </script>
   </body>
</html>