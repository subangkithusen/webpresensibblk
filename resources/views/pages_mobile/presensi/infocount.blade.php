<!DOCTYPE html>
<html lang="zxx">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="icon" href="images/favicon.png">
	<title>info presensi</title>

	<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900&display=swap" rel="stylesheet">

	{{-- <link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/ionicons.min.css">
	<link rel="stylesheet" href="css/fakeLoader.css">
	<link rel="stylesheet" href="css/swiper.min.css">
	<link rel="stylesheet" href="css/style.css"> --}}

    @include('layout_mobile.css')

</head>
<body>
	
	<!-- fakeloader -->
	<div class="fakeLoader"></div>
	<!-- end fakeloader -->
	
	<!-- navbar -->
	<div class="navbar">
		<div class="left">
			<a href="" class="link link-back"><i class="icon ion-ios-arrow-back"></i></a>
		</div>
		<div class="title">
			Info Presensi
		</div>
		<div class="right">
			
		</div>
	</div>
	<!-- end navbar -->

	<!-- pages wrapper -->
	<div class="pages-wrapper">
		<!-- separator -->
		<div class="separator-large"></div>
		<!-- end separator -->
		<!-- maintenance -->
		<div class="maintenance">
			<div class="container">
				<img src="images/maintenance.png" alt="">
				<h5 class="mb-2">Informasi</h5>
				<p class="text-small">Sisa Presensi hari ini sudah maksimal anda bisa melakukan presensi pada mesin yang ada di lobby</p>
				<div class="button-default">
					<div class="row">
						<div class="col-12">
							<a href="{{URL::to('/halaman-utama')}}" class="button btn-primary">KEMBALI</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- end maintenance -->
	</div>
	<!-- end pages wrapper -->
    @include('layout_mobile.js')

	{{-- <script src="js/jquery-3.4.1.min.js"></script>
	<script src="js/bootstrap.bundle.min.js"></script>
	<script src="js/fakeLoader.js"></script>
	<script src="js/swiper.min.js"></script>
	<script src="js/main.js"></script> --}}

</body>
</html>