<!DOCTYPE html>
<html lang="zxx">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">

	<link rel="icon" href="images/favicon.png">
	<title>App Presensi</title>

	<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900&display=swap" rel="stylesheet">
    {{-- <link rel="stylesheet" href="https://unpkg.com/leaflet@1.9.4/dist/leaflet.css"
    integrity="sha256-p4NxAoJBhIIN+hmNHrzRCf9tD/miZyoHS5obTRR9BMY="
    crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.9.4/dist/leaflet.js"
     integrity="sha256-20nQCchB9co0qIjJZRGuk2/Z9VM+kNiyxNV1lvTlZBo="
     crossorigin=""></script> --}}

	@include('layout_mobile.css')
    

</head>

<style type="text/css">
#map { height: 180px; }



</style>
<body>
	
	<!-- fakeloader -->
	<div class="fakeLoader"></div>
	<!-- end fakeloader -->

	<!-- page wrapper -->
	<div class="page-wrapper">

		{{-- <div class="section-title title-large">
			<span class="overline-title">Hi, Subangkit!</span>
			<h5>Manager Operasional</h5>
		</div> --}}

		<!-- intro app -->
        {{-- navbar --}}
        <div class="navbar navbar-home navbar-left-title bg-light">
            <div class="left">
                <p style="margin-right:10px;font-size:14px;color:#fe2775">BBLKM Surabaya</p>
                {{-- <div class="title title-home">BBLKM Surabaya</div> --}}
            </div>
            <div class="right">
                <p style="margin-right:10px;font-size:10px"></p>
                {{-- <a href="about-us.html" class="link"><img src="{{asset('mobile_theme/images/square6.jpg')}}" alt=""></a> --}}
                <svg fill="#ffffff" width="16px" height="16px" viewBox="-6.4 -6.4 44.80 44.80" xmlns="http://www.w3.org/2000/svg" stroke="#ffffff" stroke-width="0.00032"><g id="SVGRepo_bgCarrier" stroke-width="0"><rect x="-6.4" y="-6.4" width="44.80" height="44.80" rx="22.4" fill="#fe2775" strokewidth="0"></rect></g><g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g><g id="SVGRepo_iconCarrier"> <title></title> <g data-name="Layer 2" id="Layer_2"> <path d="M16,14a6,6,0,1,1,6-6A6,6,0,0,1,16,14ZM16,4a4,4,0,1,0,4,4A4,4,0,0,0,16,4Z"></path> <path d="M24,30H8a2,2,0,0,1-2-2V22a7,7,0,0,1,7-7h6a7,7,0,0,1,7,7v6A2,2,0,0,1,24,30ZM13,17a5,5,0,0,0-5,5v6H24V22a5,5,0,0,0-5-5Z"></path> </g> </g></svg>
            </div>
        </div>
        {{-- navbar --}}
		<div class="intro-app">

            <div class="container">
                <div class="row" style="margin-bottom: 10px;margin-top:60px">
                    <div class="col-12">
                        <div class="card" style="background-color:azure">
                            <div class="card-body">
                                <h5 style="color:grey">
                                    Halaman Cuti
                                </h5>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row" style="margin-bottom: 10px;margin-top:10px">
                    {{-- @foreach ($c_cuti as $item) --}}
                    <div class="col-6">
                        <a href="{{URL::to('/halaman-utama/create')}}" class="gallery-popup">
                            <div class="card card-highlight" style="height:80px">
                                <div class="card-body">
                                    <h5 class="card-title card-title-large"><center>
                                        <ion-icon name="calendar"></ion-icon>
                                    </h5>
                                    <!-- separator -->
                                    <!-- end separator -->
                                    <h5><center style="color:white">
                                    <i class="icon ion-ios-calendar" ></i> Cuti Tahunan
                                    </center></h5>
                                    
                                </div>
                            </div>
                        </a>
                    </div>
                    {{-- @endforeach --}}

                    {{-- open --}}
                    <div class="col-6">
                        <a href="{{URL::to('/halaman-utama/create')}}" class="gallery-popup">
                            <div class="card" style="background-color: #1dd1a1;height:80px">
                                <div class="card-body">
                                    <h5 class="card-title card-title-large">
                                    <center>
                                        <ion-icon name="calendar"></ion-icon>
                                    </h5>
                                    <!-- separator -->
                                    <!-- end separator -->
                                    <h5><center style="color:white">
                                    <i class="icon ion-ios-medkit" ></i> Cuti Sakit
                                    </center></h5>
                                    
                                </div>
                            </div>
                        </a>
                    </div>
                    {{-- end --}}


                    {{-- open --}}
                    {{-- <div class="col-4">
                        <a href="{{URL::to('/halaman-utama/create')}}" class="gallery-popup">
                            <div class="card card-highlight">
                                <div class="card-body">
                                    <h5 class="card-title card-title-large">
                                    <center>
                                        <ion-icon name="calendar"></ion-icon>
                                    </h5>
                                    <!-- separator -->
                                    <!-- end separator -->
                                    <h5><center style="color:white">
                                    <i class="icon ion-ios-calendar" ></i> Cuti Melahirkan
                                    </center></h5>
                                    
                                </div>
                            </div>
                        </a>
                    </div> --}}
                    {{-- end --}}
                    
                </div>


                <div class="row" style="margin-bottom: 10px;margin-top:10px">
                    {{-- @foreach ($c_cuti as $item) --}}
                    <div class="col-6">
                        <a href="{{URL::to('/halaman-utama/create')}}" class="gallery-popup">
                            <div class="card" style="background-color: #ff6b6b;height:80px">
                                <div class="card-body">
                                    <h5 class="card-title card-title-large"><center>
                                        <ion-icon name="calendar"></ion-icon>
                                    </h5>
                                    <!-- separator -->
                                    <!-- end separator -->
                                    <h5><center style="color:white">
                                    <i class="icon ion-ios-woman" ></i> Cuti Melahikan
                                    </center></h5>
                                    
                                </div>
                            </div>
                        </a>
                    </div>
                    {{-- @endforeach --}}

                    {{-- open --}}
                    <div class="col-6">
                        <a href="{{URL::to('/halaman-utama/create')}}" class="gallery-popup">
                            <div class="card" style="background-color: #5f27cd;height:80px">
                                <div class="card-body">
                                    <h5 class="card-title card-title-large">
                                    <center>
                                        <ion-icon name="calendar"></ion-icon>
                                    </h5>
                                    <!-- separator -->
                                    <!-- end separator -->
                                    <h5><center style="color:white">
                                    <i class="icon ion-ios-mail-unread" ></i> Cuti Alasan Penting
                                    </center></h5>
                                    
                                </div>
                            </div>
                        </a>
                    </div>
                    {{-- end --}}


                    {{-- open --}}
                    {{-- <div class="col-4">
                        <a href="{{URL::to('/halaman-utama/create')}}" class="gallery-popup">
                            <div class="card card-highlight">
                                <div class="card-body">
                                    <h5 class="card-title card-title-large">
                                    <center>
                                        <ion-icon name="calendar"></ion-icon>
                                    </h5>
                                    <!-- separator -->
                                    <!-- end separator -->
                                    <h5><center style="color:white">
                                    <i class="icon ion-ios-calendar" ></i> Cuti Melahirkan
                                    </center></h5>
                                    
                                </div>
                            </div>
                        </a>
                    </div> --}}
                    {{-- end --}}
                    
                </div>


                <div class="row" style="margin-bottom: 10px;margin-top:10px">
                    {{-- @foreach ($c_cuti as $item) --}}
                    <div class="col-6">
                        <a href="{{URL::to('/halaman-utama/create')}}" class="gallery-popup">
                            <div class="card" style="background-color:#00d2d3;height:80px">
                                <div class="card-body">
                                    <h5 class="card-title card-title-large"><center>
                                        <ion-icon name="calendar"></ion-icon>
                                    </h5>
                                    <!-- separator -->
                                    <!-- end separator -->
                                    <h5><center style="color:white">
                                    <i class="icon ion-ios-journal" ></i> Cuti diluartanggung
                                    </center></h5>
                                    
                                </div>
                            </div>
                        </a>
                    </div>
                    {{-- @endforeach --}}

                    {{-- open --}}
                    <div class="col-6">
                        <a href="{{URL::to('/halaman-utama/create')}}" class="gallery-popup">
                            <div class="card" style="background-color: #54a0ff;height:80px">
                                <div class="card-body">
                                    <h5 class="card-title card-title-large">
                                    <center>
                                        <ion-icon name="calendar"></ion-icon>
                                    </h5>
                                    <!-- separator -->
                                    <!-- end separator -->
                                    <h5><center style="color:white">
                                    <i class="icon ion-ios-calendar" ></i> Cuti Besar
                                    </center></h5>
                                    
                                </div>
                            </div>
                        </a>
                    </div>
                    {{-- end --}}


                    {{-- open --}}
                    {{-- <div class="col-4">
                        <a href="{{URL::to('/halaman-utama/create')}}" class="gallery-popup">
                            <div class="card card-highlight">
                                <div class="card-body">
                                    <h5 class="card-title card-title-large">
                                    <center>
                                        <ion-icon name="calendar"></ion-icon>
                                    </h5>
                                    <!-- separator -->
                                    <!-- end separator -->
                                    <h5><center style="color:white">
                                    <i class="icon ion-ios-calendar" ></i> Cuti Melahirkan
                                    </center></h5>
                                    
                                </div>
                            </div>
                        </a>
                    </div> --}}
                    {{-- end --}}
                    
                </div>




            </div>


			
		</div>

	</div>
	<!-- end page wrapper -->

	<!-- toolbar bottom -->
	@include('layout_mobile.menubawa')
	<!-- end toolbar bottom -->

	@include('layout_mobile.js')



</body>
</html>