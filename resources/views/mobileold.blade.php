<!DOCTYPE html>
<html lang="zxx">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

	<link rel="icon" href="images/favicon.png">
	<title>App Presensi</title>

	<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900&display=swap" rel="stylesheet">
    {{-- <link rel="stylesheet" href="https://unpkg.com/leaflet@1.9.4/dist/leaflet.css"
    integrity="sha256-p4NxAoJBhIIN+hmNHrzRCf9tD/miZyoHS5obTRR9BMY="
    crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.9.4/dist/leaflet.js"
     integrity="sha256-20nQCchB9co0qIjJZRGuk2/Z9VM+kNiyxNV1lvTlZBo="
     crossorigin=""></script> --}}

	@include('layout_mobile.css')
    

</head>

<style type="text/css">
#map { height: 180px; }



</style>
<body>
	
	<!-- fakeloader -->
	<div class="fakeLoader"></div>
	<!-- end fakeloader -->

	<!-- page wrapper -->
	<div class="page-wrapper">

		{{-- <div class="section-title title-large">
			<span class="overline-title">Hi, Subangkit!</span>
			<h5>Manager Operasional</h5>
		</div> --}}

		<!-- intro app -->
		<div class="intro-app">
            <div class="container">
                <div class="row" style="margin-bottom: 10px">
                    <div class="col-12">
                        <div class="card bg-light">
                            <div class="card-body">
                                <div class="row">
                                    <div class="card bg-light">
                                        <div class="card-body">
                                            <h6>Hai ! Yunan</h6>
                                        </div>
                                    </div>
                                </div>
                           

                                <div class="row" style="padding-bottom: 15px">
                                    <div class="col-3"><a href="#" class="gallery-popup">
                                        <img src="{{asset('mobile_theme/images/1.png')}}" alt="Presensi"><center>Presensi</center>
                                    </a></div>
                                    <div class="col-3"><a href="#" class="gallery-popup">
                                        <img src="{{asset('mobile_theme/images/2.png')}}" alt="image-demo"><center>Cuti</center>
                                    </a></div>
                                    <div class="col-3"><a href="#" class="gallery-popup">
                                        <img src="{{asset('mobile_theme/images/3.png')}}" alt="image-demo"><center>History</center>
                                    </a></div>
                                    <div class="col-3"><a href="#" class="gallery-popup">
                                        <img src="{{asset('mobile_theme/images/4.png')}}" alt="image-demo"><center>Profil</center>
                                    </a></div>
                                </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>


			<div class="container">
				<div class="row">
					<div class="col-6">
						<div class="card card-highlight">
							<div class="card-body">
								<h5 class="card-title card-title-large"><center>MASUK</center></h5>
								{{-- <h6 class="card-subtitle">Ananas comosus</h6> --}}

								<!-- separator -->
								{{-- <div class="separator-small"></div> --}}
								<!-- end separator -->

								<h5 class="card-title card-title-small"><center>
                                   @isset($datain->date_log)
                                   <b>{!!substr($datain->date_log,10,6)!!}</b>
                                   @endisset
                                </center></h5>
								{{-- <h6 class="card-location">New York, US</h6> --}}
							</div>
						</div>
					</div>
					<div class="col-6">
						<div class="card card-highlight bg-blue">
							<div class="card-body">
								<h5 class="card-title card-title-large"><center>PULANG</center></h5>
								{{-- <h6 class="card-subtitle">Ananas comosus</h6> --}}

								<!-- separator -->
								{{-- <div class="separator-small"></div> --}}
								<!-- end separator -->

								<h5 class="card-title card-title-small"><center>
                                    @isset($dataout->date_log)
                                    
                                    <b>{!!substr($dataout->date_log,10,6)!!}</b>
                                @endisset
                                    
                                </center></h5>
								{{-- <h6 class="card-location">New York, US</h6> --}}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- end intro app -->

        {{-- list history absen hari ini --}}
        {{-- <div class="container" style="margin-top:10px">
            <div class="row">
                <div class="col-12">
                    <div class="card card-highlight bg-lightblue">
                        <div class="card-body">
                            <h5 class="card-title card-title-large">Lokasi Saat Ini</h5>
                            
                            <div id="map"></div>


                        </div>
                    </div>
                </div>
            </div>
        </div> --}}
        {{-- end list history --}}


         {{-- list history absen hari ini --}}
         <div class="container" style="margin-top:10px">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h6 class="card-title card-title-large">History Absensi</h6>
                            {{-- tab --}}
                            <div class="tab-two">
                                <ul class="nav nav-fill nav-outline nav-outline-icon tab-label-active" id="myTab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="features-tab" data-toggle="tab" href="#features" role="tab" aria-controls="features" aria-selected="true">
                                            <i class="icon ion-ios-star"></i>
                                            <span class="nav-label">Presensi</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="apps-tab" data-toggle="tab" href="#apps" role="tab" aria-controls="apps" aria-selected="false">
                                            <i class="icon ion-ios-apps"></i>
                                            <span class="nav-label">Apps</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="pages-tab" data-toggle="tab" href="#pages" role="tab" aria-controls="pages" aria-selected="false">
                                            <i class="icon ion-ios-browsers"></i>
                                            <span class="nav-label">Pages</span>
                                        </a>
                                    </li>
                                </ul>
                    
                                <!-- separator -->
                                <div class="separator-large"></div>
                                <!-- end separator -->
                    
                                <div class="tab-content">
                                    <div class="tab-pane fade active show" id="features" role="tabpanel" aria-labelledby="features-tab">
                                        <div class="container">
                                            {{-- <div class="card bg-lightblue border-0">
                                                <div class="card-body">
                                                    <h5 class="card-title">Features</h5>
                                                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus, veniam.</p>
                                                </div>
                                            </div> --}}
                                        </div>
                    
                                        <!-- separator -->
                                        {{-- <div class="separator-medium"></div> --}}
                                        <!-- end separator -->
                    
                                        <div class="list-view list-colored">
                                            <ul>

                                                @foreach ($data as $dt)
                                                <li class="list-item">
                                                    <div class="list-media">
                                                        <img src="{{asset('mobile_theme/images/icon-time.png')}}" alt="Presensi"></a>
                                                        
                                                        {{-- <i class="icon ion-ios-arrow-down bg-blue"></i> --}}
                                                    </div>
                                                    <div class="list-label">
                                                        <div class="list-title">{{@$dt->PresenseEmployee->employee_name}} - <span class="badge bg-blue" style="color:white">{{@$dt->date_log}}</span></div>
                                                        {{-- <div class="list-after"><i class="icon ion-ios-arrow-forward"></i></div> --}}
                                                    </div>
                                                </li>
                                                @endforeach
                                                
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="apps" role="tabpanel" aria-labelledby="apps-tab">
                                        
                                        <div class="container">
                                            <div class="card bg-lightblue border-0">
                                                <div class="card-body">
                                                    <h5 class="card-title">Apps</h5>
                                                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus, veniam.</p>
                                                </div>
                                            </div>
                                        </div>
                    
                                        <!-- separator -->
                                        <div class="separator-medium"></div>
                                        <!-- end separator -->
                    
                                        <div class="list-view list-colored">
                                            <ul>
                                                <li class="list-item">
                                                    <div class="list-media">
                                                        <i class="icon ion-ios-chatboxes bg-red"></i>
                                                    </div>
                                                    <div class="list-label">
                                                        <div class="list-title">Chat</div>
                                                        <div class="list-after"><i class="icon ion-ios-arrow-forward"></i></div>
                                                    </div>
                                                </li>
                                                <li class="list-item">
                                                    <div class="list-media">
                                                        <i class="icon ion-ios-rocket bg-green"></i>
                                                    </div>
                                                    <div class="list-label">
                                                        <div class="list-title">Coming Soon</div>
                                                        <div class="list-after"><i class="icon ion-ios-arrow-forward"></i></div>
                                                    </div>
                                                </li>
                                                <li class="list-item">
                                                    <div class="list-media">
                                                        <i class="icon ion-ios-call bg-yellow"></i>
                                                    </div>
                                                    <div class="list-label">
                                                        <div class="list-title">Contact</div>
                                                        <div class="list-after"><i class="icon ion-ios-arrow-forward"></i></div>
                                                    </div>
                                                </li>
                                                <li class="list-item">
                                                    <div class="list-media">
                                                        <i class="icon ion-ios-images bg-purple"></i>
                                                    </div>
                                                    <div class="list-label">
                                                        <div class="list-title">Gallery</div>
                                                        <div class="list-after"><i class="icon ion-ios-arrow-forward"></i></div>
                                                    </div>
                                                </li>
                                                <li class="list-item">
                                                    <div class="list-media">
                                                        <i class="icon ion-ios-map bg-orange"></i>
                                                    </div>
                                                    <div class="list-label">
                                                        <div class="list-title">Map</div>
                                                        <div class="list-after"><i class="icon ion-ios-arrow-forward"></i></div>
                                                    </div>
                                                </li>
                                                <li class="list-item">
                                                    <div class="list-media">
                                                        <i class="icon ion-ios-reorder bg-lime"></i>
                                                    </div>
                                                    <div class="list-label">
                                                        <div class="list-title">Navbar</div>
                                                        <div class="list-after"><i class="icon ion-ios-arrow-forward"></i></div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="pages" role="tabpanel" aria-labelledby="pages-tab">
                                        
                    
                                        <div class="container">
                                            <div class="card bg-lightblue border-0">
                                                <div class="card-body">
                                                    <h5 class="card-title">Pages</h5>
                                                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus, veniam.</p>
                                                </div>
                                            </div>
                                        </div>
                    
                                        <!-- separator -->
                                        <div class="separator-medium"></div>
                                        <!-- end separator -->
                    
                                        <div class="list-view list-colored">
                                            <ul>
                                                <li class="list-item">
                                                    <div class="list-media">
                                                        <i class="icon ion-ios-information-circle bg-blue"></i>
                                                    </div>
                                                    <div class="list-label">
                                                        <div class="list-title">About Us</div>
                                                        <div class="list-after"><i class="icon ion-ios-arrow-forward"></i></div>
                                                    </div>
                                                </li>
                                                <li class="list-item">
                                                    <div class="list-media">
                                                        <i class="icon ion-ios-help-circle-outline bg-red"></i>
                                                    </div>
                                                    <div class="list-label">
                                                        <div class="list-title">Faq</div>
                                                        <div class="list-after"><i class="icon ion-ios-arrow-forward"></i></div>
                                                    </div>
                                                </li>
                                                <li class="list-item">
                                                    <div class="list-media">
                                                        <i class="icon ion-ios-chatboxes bg-green"></i>
                                                    </div>
                                                    <div class="list-label">
                                                        <div class="list-title">Forum</div>
                                                        <div class="list-after"><i class="icon ion-ios-arrow-forward"></i></div>
                                                    </div>
                                                </li>
                                                <li class="list-item">
                                                    <div class="list-media">
                                                        <i class="icon ion-ios-images bg-yellow"></i>
                                                    </div>
                                                    <div class="list-label">
                                                        <div class="list-title">Gallery</div>
                                                        <div class="list-after"><i class="icon ion-ios-arrow-forward"></i></div>
                                                    </div>
                                                </li>
                                                <li class="list-item">
                                                    <div class="list-media">
                                                        <i class="icon ion-ios-build bg-purple"></i>
                                                    </div>
                                                    <div class="list-label">
                                                        <div class="list-title">Maintenance</div>
                                                        <div class="list-after"><i class="icon ion-ios-arrow-forward"></i></div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                    
                            </div>

                            {{-- end tab --}}

                        </div>
                    </div>
                </div>
            </div>
        </div>


        {{-- end list history --}}

		<!-- feature slide -->
            {{-- <div class="feature-slide">
                <div class="swiper-container swiper-style">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="content shadow-sm">
                                <div class="info-statistic bg-blue">10+</div>
                                <div class="icon"><i class="icon ion-ios-star color-blue"></i></div>
                                <div class="text">
                                    <h5>Features</h5>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="content shadow-sm">
                                <div class="info-statistic bg-red">9+</div>
                                <div class="icon"><i class="icon ion-ios-apps color-red"></i></div>
                                <div class="text">
                                    <h5>Apps</h5>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="content shadow-sm">
                                <div class="info-statistic bg-yellow">12+</div>
                                <div class="icon"><i class="icon ion-ios-browsers color-yellow"></i></div>
                                <div class="text">
                                    <h5>Pages</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> --}}

		<!-- feature list -->
		{{-- <div class="feature-list">
			<div class="swiper-container swiper-style-full">
				<div class="swiper-wrapper">
					<div class="swiper-slide">
						<div class="content bg-lightblue">
							<div class="wrap-icon">
								<i class="icon ion-ios-brush bg-blue"></i>
							</div>
							<div class="text">
								<h5>Easy to Edit</h5>
								<p>With a neat page layout and well arranged code</p>
							</div>
						</div>
					</div>
					<div class="swiper-slide">
						<div class="content bg-lightgreen">
							<div class="wrap-icon">
								<i class="icon ion-ios-checkbox bg-green"></i>
							</div>
							<div class="text">
								<h5>Complete Features</h5>
								<p>Equipped with various features such as lists, buttons, and others</p>
							</div>
						</div>
					</div>
					<div class="swiper-slide">
						<div class="content bg-lightred">
							<div class="wrap-icon">
								<i class="icon ion-ios-keypad bg-red"></i>
							</div>
							<div class="text">
								<h5>Well Documented</h5>
								<p>So that users easily use and customize this template</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div> --}}
		<!-- feature list -->

		<!-- separator -->
		{{-- <div class="separator-large"></div> --}}
		<!-- end separator -->

		<!-- newsletter -->
		{{-- <div class="newsletter bg-lightblue"> --}}

			<!-- separator -->
			<div class="separator-medium"></div>
			<!-- end separator -->

			{{-- <div class="section-title text-center title-large">
				<h3>Subscribe Newsletter</h3>
				<p class="text-small mt-1">Enter you email to recieive newsletters</p>
			</div>
			<div class="container">

				<form class="form-fill form-icon-inset">
					<div class="form-wrapper">
						<div class="input-wrap bg-white">
							<input type="email" placeholder="Email" class="bg-white">
							<a href="#">
								<div class="button-icon">
									<i class="icon ion-ios-checkmark"></i>
								</div>
							</a>
						</div>
					</div>
				</form>
			</div> --}}

			<!-- separator -->
			<div class="separator-large"></div>
			<!-- end separator -->

		{{-- </div> --}}
		<!-- end newsletter -->

		<!-- separator -->
		{{-- <div class="separator-small"></div> --}}
		<!-- end separator -->

		<!-- footer -->
		{{-- <div class="footer">
			<div class="container">
				<div class="content-box shadow-sm">
					<h4 class="mb-1">Maxui</h4>
					<span>Mobile Template</span>

					<!-- separator -->
					<div class="separator-small"></div>
					<!-- end separator -->

					<div class="social-media-icon">
						<ul>
							<li><a href="#"><i class="icon ion-logo-facebook bg-facebook"></i></a></li>
							<li><a href="#"><i class="icon ion-logo-instagram bg-instagram"></i></a></li>
							<li><a href="#"><i class="icon ion-logo-twitter bg-twitter"></i></a></li>
							<li><a href="#"><i class="icon ion-logo-whatsapp bg-whatsapp"></i></a></li>
						</ul>
					</div>

					<!-- separator -->
					<div class="separator-small"></div>
					<!-- end separator -->

					<p class="made-by text-small">Made With <i class="icon ion-ios-heart"></i> aStylers</p>
				</div>
			</div>
		</div> --}}
		<!-- end footer -->

		<!-- separator -->
		<div class="separator-large"></div>
		<!-- end separator -->

	</div>
	<!-- end page wrapper -->

	<!-- toolbar bottom -->
	<div class="toolbar">
		<div class="container">
			<ul class="toolbar-bottom toolbar-wrap">
				<li class="toolbar-item">
					<a href="index.html" class="toolbar-link toolbar-link-active">
						<i class="icon ion-ios-home"></i>
					</a>
				</li>
				<li class="toolbar-item">
					<a href="features.html" class="toolbar-link">
						<i class="icon ion-ios-star"></i>
					</a>
				</li>
				<li class="toolbar-item">
					<a href="pages.html" class="toolbar-link">
						<i class="icon ion-ios-browsers"></i>
					</a>
				</li>
				<li class="toolbar-item">
					<a href="apps.html" class="toolbar-link">
						<i class="icon ion-ios-apps"></i>
					</a>
				</li>
			</ul>
		</div>
	</div>
	<!-- end toolbar bottom -->

	@include('layout_mobile.js')
    
    {{-- <script>

        // fungsi harvsine
        function haversine(lat1, lon1, lat2, lon2) {
        const R = 6371; // Radius bumi dalam kilometer
        const dLat = (lat2 - lat1) * (Math.PI / 180);
        const dLon = (lon2 - lon1) * (Math.PI / 180);
        const a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(lat1 * (Math.PI / 180)) * Math.cos(lat2 * (Math.PI / 180)) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
        const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        const distance = R * c; // Jarak dalam kilometer
        return distance;
        }

        var mapOptions = {
        center: [-7.2684167, 112.7603571],
        zoom: 10
        }

       

        var map = new L.map('map', mapOptions);            
        var layer = new L.TileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');               
        map.addLayer(layer);

       
        var circle = L.circle([-7.2684167, 112.7603571], {
        color: 'red'
        , fillColor: '#f03'
        , fillOpacity: 0.5
        , radius: 50
        }).addTo(map).bindPopup('Kantor BBLK Surabaya');

        // ambil data dari javascript
        const options = {
        enableHighAccuracy: true, 
        // Get high accuracy reading, if available (default false)
        timeout: 5000, 
        // Time to return a position successfully before error (default infinity)
        maximumAge: 2000, 
        // Milliseconds for which it is acceptable to use cached position (default 0)
        };

        navigator.geolocation.watchPosition(success, error, options);
        // Fires success function immediately and when user position changes
        function success(pos) {
        const lat = pos.coords.latitude;
        const lng = pos.coords.longitude;
        const accuracy = pos.coords.accuracy; // Accuracy in metres
        var marker = L.marker([lat,lng]).addTo(map).bindPopup('Lokasi Saya saat ini');
        }

        function error(err) {
        if (err.code === 1) {
        alert("Please allow geolocation access");
        // Runs if user refuses access
        } else {
        // console.log("lokasi tidak dapat di ambil");
        alert("Cannot get current location");
        // Runs if there was a technical problem.
        }
        }

        





        

    </script> --}}


</body>
</html>