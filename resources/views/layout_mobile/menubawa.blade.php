<div class="toolbar">
    <div class="container">
        <ul class="toolbar-bottom toolbar-wrap">
            <li class="toolbar-item">
                <a href="{{URL::to('/halaman-utama')}}" class="toolbar-link toolbar-link-active">
                    <i class="icon ion-ios-home"></i>
                </a>
            </li>
            <li class="toolbar-item">
                <a href="{{URL::to('/halaman-utama/create')}}" class="toolbar-link">
                    <i class="icon ion-ios-finger-print"></i>
                </a>
            </li>
            {{-- <li class="toolbar-item">
                <a href="pages.html" class="toolbar-link">
                    <i class="icon ion-ios-browsers"></i>
                </a>
            </li> --}}
            <li class="toolbar-item">
                <a class="toolbar-link" href="{{ route('logout') }}"
                onclick="event.preventDefault();
                              document.getElementById('logout-form').submit();">
                 {{-- {{ __('Logout') }} --}}
                    <i class="icon ion-ios-log-out"></i>
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>
        </ul>
    </div>
</div>