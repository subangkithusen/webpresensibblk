<!DOCTYPE html>
<html lang="zxx">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">

	<link rel="icon" href="images/favicon.png">
	<title>App Presensi</title>

	<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900&display=swap" rel="stylesheet">
    {{-- <link rel="stylesheet" href="https://unpkg.com/leaflet@1.9.4/dist/leaflet.css"
    integrity="sha256-p4NxAoJBhIIN+hmNHrzRCf9tD/miZyoHS5obTRR9BMY="
    crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.9.4/dist/leaflet.js"
     integrity="sha256-20nQCchB9co0qIjJZRGuk2/Z9VM+kNiyxNV1lvTlZBo="
     crossorigin=""></script> --}}

	@include('layout_mobile.css')
    

</head>

<style type="text/css">
#map { height: 180px; }



</style>
<body>
	
	<!-- fakeloader -->
	<div class="fakeLoader"></div>
	<!-- end fakeloader -->

	<!-- page wrapper -->
	<div class="page-wrapper">

		{{-- <div class="section-title title-large">
			<span class="overline-title">Hi, Subangkit!</span>
			<h5>Manager Operasional</h5>
		</div> --}}

		<!-- intro app -->
        {{-- navbar --}}
        <div class="navbar navbar-home navbar-left-title bg-light">
            <div class="left">
                <p style="margin-right:10px;font-size:14px;color:#fe2775">BBLKM Surabaya</p>
                {{-- <div class="title title-home">BBLKM Surabaya</div> --}}
            </div>
            <div class="right">
                <p style="margin-right:10px;font-size:10px">{{@$userdata->name}}</p>
                {{-- <a href="about-us.html" class="link"><img src="{{asset('mobile_theme/images/square6.jpg')}}" alt=""></a> --}}
                <svg fill="#ffffff" width="16px" height="16px" viewBox="-6.4 -6.4 44.80 44.80" xmlns="http://www.w3.org/2000/svg" stroke="#ffffff" stroke-width="0.00032"><g id="SVGRepo_bgCarrier" stroke-width="0"><rect x="-6.4" y="-6.4" width="44.80" height="44.80" rx="22.4" fill="#fe2775" strokewidth="0"></rect></g><g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g><g id="SVGRepo_iconCarrier"> <title></title> <g data-name="Layer 2" id="Layer_2"> <path d="M16,14a6,6,0,1,1,6-6A6,6,0,0,1,16,14ZM16,4a4,4,0,1,0,4,4A4,4,0,0,0,16,4Z"></path> <path d="M24,30H8a2,2,0,0,1-2-2V22a7,7,0,0,1,7-7h6a7,7,0,0,1,7,7v6A2,2,0,0,1,24,30ZM13,17a5,5,0,0,0-5,5v6H24V22a5,5,0,0,0-5-5Z"></path> </g> </g></svg>
            </div>
        </div>
        {{-- navbar --}}
		<div class="intro-app">
            <div class="container">
                <div class="row" style="margin-bottom: 10px;margin-top:60px">
                    <div class="col-12">
                        <div class="card bg-light">
                            <div class="card-body">
                                <div class="row">

                                    {{-- <div class="left">
                                        <h6>BBLKM Surabaya</h6>
                                        
                                    </div>

                                    <div class="right">
                                        <h6>Hai ! Yunan</h6>
                                        <a href="about-us.html" class="link"><img src="images/square6.jpg" alt=""></a>

                                    </div> --}}
                                    {{-- <div class="card bg-light">
                                        <div class="card-body">
                                            
                                        </div>
                                    </div> --}}
                                    
                                </div>
                           

                                <div class="row" style="padding-bottom: 15px">
                                    <div class="col-6"><a href="{{URL::to('/halaman-utama/create')}}" class="gallery-popup">
                                        <div class="card card-highlight">
                                            <div class="card-body">
                                                <h5 class="card-title card-title-large"><center>
                                                    <svg fill="#ffffff" width="64px" height="64px" viewBox="-51.2 -51.2 614.40 614.40" xmlns="http://www.w3.org/2000/svg" stroke="#ffffff"><g id="SVGRepo_bgCarrier" stroke-width="0"><rect x="-51.2" y="-51.2" width="614.40" height="614.40" rx="307.2" fill="#fe2775" strokewidth="0"></rect></g><g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g><g id="SVGRepo_iconCarrier"><title>ionicons-v5-i</title><path d="M63.28,202a15.29,15.29,0,0,1-7.7-2,14.84,14.84,0,0,1-5.52-20.46C69.34,147.36,128,72.25,256,72.25c55.47,0,104.12,14.57,144.53,43.29,33.26,23.57,51.9,50.25,60.78,63.1a14.79,14.79,0,0,1-4,20.79,15.52,15.52,0,0,1-21.24-4C420,172.32,371,102,256,102c-112.25,0-163,64.71-179.53,92.46A15,15,0,0,1,63.28,202Z"></path><path d="M320.49,496a15.31,15.31,0,0,1-3.79-.43c-92.85-23-127.52-115.82-128.93-119.68l-.22-.85c-.76-2.68-19.39-66.33,9.21-103.61,13.11-17,33.05-25.72,59.38-25.72,24.48,0,42.14,7.61,54.28,23.36,10,12.86,14,28.72,17.87,44,8.13,31.82,14,48.53,47.79,50.25,14.84.75,24.59-7.93,30.12-15.32,14.95-20.15,17.55-53,6.28-82C398,228.57,346.61,158,256,158c-38.68,0-74.22,12.43-102.72,35.79C129.69,213.14,111,240.46,102,268.54c-16.69,52.28,5.2,134.46,5.41,135.21A14.83,14.83,0,0,1,96.54,422a15.39,15.39,0,0,1-18.74-10.6c-1-3.75-24.38-91.4-5.1-151.82,21-65.47,85.81-131.47,183.33-131.47,45.07,0,87.65,15.32,123.19,44.25,27.52,22.5,50,52.72,61.76,82.93,14.95,38.57,10.94,81.86-10.19,110.14-14.08,18.86-34.13,28.72-56.34,27.65-57.86-2.9-68.26-43.29-75.84-72.75-7.8-30.22-12.79-44.79-42.58-44.79-16.36,0-27.85,4.5-35,13.82-9.75,12.75-10.51,32.68-9.43,47.14a152.44,152.44,0,0,0,5.1,29.79c2.38,6,33.37,82,107.59,100.39a14.88,14.88,0,0,1,11,18.11A15.36,15.36,0,0,1,320.49,496Z"></path><path d="M201.31,489.14a15.5,15.5,0,0,1-11.16-4.71c-37.16-39-58.18-82.61-66.09-137.14V347c-4.44-36.1,2.06-87.21,33.91-122.35,23.51-25.93,56.56-39.11,98.06-39.11,49.08,0,87.65,22.82,111.7,65.89,17.45,31.29,20.91,62.47,21,63.75a15.07,15.07,0,0,1-13.65,16.4,15.26,15.26,0,0,1-16.79-13.29h0A154,154,0,0,0,340.43,265c-18.64-32.89-47-49.61-84.51-49.61-32.4,0-57.75,9.75-75.19,29-25.14,27.75-30,70.5-26.55,98.78,6.93,48.22,25.46,86.58,58.18,120.86a14.7,14.7,0,0,1-.76,21.11A15.44,15.44,0,0,1,201.31,489.14Z"></path><path d="M372.5,446.18c-32.5,0-60.13-9-82.24-26.89-44.42-35.79-49.4-94.08-49.62-96.54a15.27,15.27,0,0,1,30.45-2.36c.11.86,4.55,48.54,38.79,76,20.26,16.18,47.34,22.6,80.71,18.85a15.2,15.2,0,0,1,16.91,13.18,14.92,14.92,0,0,1-13.44,16.5A187,187,0,0,1,372.5,446.18Z"></path><path d="M398.18,48.79C385.5,40.54,340.54,16,256,16c-88.74,0-133.81,27.11-143.78,34a11.59,11.59,0,0,0-1.84,1.4.36.36,0,0,1-.22.1,14.87,14.87,0,0,0-5.09,11.15A15.06,15.06,0,0,0,120.38,77.5a15.56,15.56,0,0,0,8.88-2.79c.43-.32,39.22-28.82,126.77-28.82S382.58,74.29,383,74.5a15.25,15.25,0,0,0,9.21,3A15.06,15.06,0,0,0,407.5,62.61,14.9,14.9,0,0,0,398.18,48.79Z"></path></g></svg>
                                                </center></h5>
                                                <!-- separator -->
                                                <!-- end separator -->
                
                                                <h5 class="card-title card-title-small"><center>
                                                                                      <b>Presensi</b> 
                                                </center></h5>
                                                
                                            </div>
                                        </div>
                                    </a></div>
                                    {{-- <div class="col-4"><a href="#" class="gallery-popup">
                                        <div class="card card-highlight">
                                            <div class="card-body">
                                                <h5 class="card-title card-title-large"><center>
                                                    <svg fill="#ffffff" width="64px" height="64px" viewBox="-307.2 -307.2 1638.40 1638.40" xmlns="http://www.w3.org/2000/svg" stroke="#ffffff"><g id="SVGRepo_bgCarrier" stroke-width="0"><rect x="-307.2" y="-307.2" width="1638.40" height="1638.40" rx="819.2" fill="#fe2775" strokewidth="0"></rect></g><g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g><g id="SVGRepo_iconCarrier"><path d="M960 95.888l-256.224.001V32.113c0-17.68-14.32-32-32-32s-32 14.32-32 32v63.76h-256v-63.76c0-17.68-14.32-32-32-32s-32 14.32-32 32v63.76H64c-35.344 0-64 28.656-64 64v800c0 35.343 28.656 64 64 64h896c35.344 0 64-28.657 64-64v-800c0-35.329-28.656-63.985-64-63.985zm0 863.985H64v-800h255.776v32.24c0 17.679 14.32 32 32 32s32-14.321 32-32v-32.224h256v32.24c0 17.68 14.32 32 32 32s32-14.32 32-32v-32.24H960v799.984zM736 511.888h64c17.664 0 32-14.336 32-32v-64c0-17.664-14.336-32-32-32h-64c-17.664 0-32 14.336-32 32v64c0 17.664 14.336 32 32 32zm0 255.984h64c17.664 0 32-14.32 32-32v-64c0-17.664-14.336-32-32-32h-64c-17.664 0-32 14.336-32 32v64c0 17.696 14.336 32 32 32zm-192-128h-64c-17.664 0-32 14.336-32 32v64c0 17.68 14.336 32 32 32h64c17.664 0 32-14.32 32-32v-64c0-17.648-14.336-32-32-32zm0-255.984h-64c-17.664 0-32 14.336-32 32v64c0 17.664 14.336 32 32 32h64c17.664 0 32-14.336 32-32v-64c0-17.68-14.336-32-32-32zm-256 0h-64c-17.664 0-32 14.336-32 32v64c0 17.664 14.336 32 32 32h64c17.664 0 32-14.336 32-32v-64c0-17.68-14.336-32-32-32zm0 255.984h-64c-17.664 0-32 14.336-32 32v64c0 17.68 14.336 32 32 32h64c17.664 0 32-14.32 32-32v-64c0-17.648-14.336-32-32-32z"></path></g></svg>
                                                </center></h5>
                                                <h5 class="card-title card-title-small"><center>
                                                                                      <b>Cuti</b> 
                                                </center></h5>
                                                
                                            </div>
                                        </div>
                                        </a>
                                    </div> --}}
                                    {{-- <div class="col-3"><a href="#" class="gallery-popup">
                                        <img src="{{asset('mobile_theme/images/3.png')}}" alt="image-demo"><center>Laporan</center>
                                    </a></div> --}}
                                    <div class="col-6"><a href="{{URL::to('/profile')}}" class="gallery-popup">
                                        <div class="card card-highlight">
                                            <div class="card-body">
                                                <h5 class="card-title card-title-large"><center>
                                                    <svg fill="#ffffff" width="64px" height="64px" viewBox="-6.4 -6.4 44.80 44.80" xmlns="http://www.w3.org/2000/svg" stroke="#ffffff" stroke-width="0.00032"><g id="SVGRepo_bgCarrier" stroke-width="0"><rect x="-6.4" y="-6.4" width="44.80" height="44.80" rx="22.4" fill="#fe2775" strokewidth="0"></rect></g><g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g><g id="SVGRepo_iconCarrier"> <title></title> <g data-name="Layer 2" id="Layer_2"> <path d="M16,14a6,6,0,1,1,6-6A6,6,0,0,1,16,14ZM16,4a4,4,0,1,0,4,4A4,4,0,0,0,16,4Z"></path> <path d="M24,30H8a2,2,0,0,1-2-2V22a7,7,0,0,1,7-7h6a7,7,0,0,1,7,7v6A2,2,0,0,1,24,30ZM13,17a5,5,0,0,0-5,5v6H24V22a5,5,0,0,0-5-5Z"></path> </g> </g></svg>
                                                </center></h5>
                                                <!-- separator -->
                                                <!-- end separator -->
                
                                                <h5 class="card-title card-title-small"><center>
                                                                                      <b>Profile</b> 
                                                </center></h5>
                                                
                                            </div>
                                        </div>
                                        {{-- <center>Profil</center> --}}
                                        {{-- <img src="{{asset('mobile_theme/images/4.png')}}" alt="image-demo"><center>Profil</center> --}}
                                    </a></div>
                                </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>


			<div class="container">
				<div class="row">
					<div class="col-6">
						<div class="card card-highlight" style="background-color:#0b7845">
							<div class="card-body">
								<h5 class="card-title card-title-large"><center>MASUK</center></h5>
								{{-- <h6 class="card-subtitle">Ananas comosus</h6> --}}

								<!-- separator -->
								{{-- <div class="separator-small"></div> --}}
								<!-- end separator -->

								<h5 class="card-title card-title-small"><center>
                                    
                                  
                                   @if(isset($datain->date_log))

                                   <b> <i class="icon ion-ios-log-in"></i> {!!substr($datain->date_log,10,6)!!}</b>
                                   @else
                                   <b>-</b>
                                       
                                   @endif

                                  
                                </center></h5>
								
							</div>
						</div>
					</div>
					<div class="col-6">
						<div class="card card-highlight" style="background-color:#780B3E">
							<div class="card-body">
								<h5 class="card-title card-title-large"><center>PULANG</center></h5>
								<h5 class="card-title card-title-small"><center>  
                                @if(isset($dataout->date_log))
                                    <b><i class="icon ion-ios-log-out"></i> {!!substr($dataout->date_log,10,6)!!}</b>
                                @else
                                    <b>-</b>    
                                @endif    
                                </center></h5>
								{{-- <h6 class="card-location">New York, US</h6> --}}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- end intro app -->
         <div class="container" style="margin-top:10px">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h6 class="card-title card-title-large">History Absensi</h6>
                            {{-- tab --}}
                            <div class="tab-two">
                                <ul class="nav nav-fill nav-outline nav-outline-icon tab-label-active" id="myTab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="features-tab" data-toggle="tab" href="#features" role="tab" aria-controls="features" aria-selected="true">
                                            <svg fill="#ffffff" width="32px" height="32px" viewBox="-51.2 -51.2 614.40 614.40" xmlns="http://www.w3.org/2000/svg" stroke="#ffffff"><g id="SVGRepo_bgCarrier" stroke-width="0"><rect x="-51.2" y="-51.2" width="614.40" height="614.40" rx="307.2" fill="#fe2775" strokewidth="0"></rect></g><g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g><g id="SVGRepo_iconCarrier"><title>ionicons-v5-i</title><path d="M63.28,202a15.29,15.29,0,0,1-7.7-2,14.84,14.84,0,0,1-5.52-20.46C69.34,147.36,128,72.25,256,72.25c55.47,0,104.12,14.57,144.53,43.29,33.26,23.57,51.9,50.25,60.78,63.1a14.79,14.79,0,0,1-4,20.79,15.52,15.52,0,0,1-21.24-4C420,172.32,371,102,256,102c-112.25,0-163,64.71-179.53,92.46A15,15,0,0,1,63.28,202Z"></path><path d="M320.49,496a15.31,15.31,0,0,1-3.79-.43c-92.85-23-127.52-115.82-128.93-119.68l-.22-.85c-.76-2.68-19.39-66.33,9.21-103.61,13.11-17,33.05-25.72,59.38-25.72,24.48,0,42.14,7.61,54.28,23.36,10,12.86,14,28.72,17.87,44,8.13,31.82,14,48.53,47.79,50.25,14.84.75,24.59-7.93,30.12-15.32,14.95-20.15,17.55-53,6.28-82C398,228.57,346.61,158,256,158c-38.68,0-74.22,12.43-102.72,35.79C129.69,213.14,111,240.46,102,268.54c-16.69,52.28,5.2,134.46,5.41,135.21A14.83,14.83,0,0,1,96.54,422a15.39,15.39,0,0,1-18.74-10.6c-1-3.75-24.38-91.4-5.1-151.82,21-65.47,85.81-131.47,183.33-131.47,45.07,0,87.65,15.32,123.19,44.25,27.52,22.5,50,52.72,61.76,82.93,14.95,38.57,10.94,81.86-10.19,110.14-14.08,18.86-34.13,28.72-56.34,27.65-57.86-2.9-68.26-43.29-75.84-72.75-7.8-30.22-12.79-44.79-42.58-44.79-16.36,0-27.85,4.5-35,13.82-9.75,12.75-10.51,32.68-9.43,47.14a152.44,152.44,0,0,0,5.1,29.79c2.38,6,33.37,82,107.59,100.39a14.88,14.88,0,0,1,11,18.11A15.36,15.36,0,0,1,320.49,496Z"></path><path d="M201.31,489.14a15.5,15.5,0,0,1-11.16-4.71c-37.16-39-58.18-82.61-66.09-137.14V347c-4.44-36.1,2.06-87.21,33.91-122.35,23.51-25.93,56.56-39.11,98.06-39.11,49.08,0,87.65,22.82,111.7,65.89,17.45,31.29,20.91,62.47,21,63.75a15.07,15.07,0,0,1-13.65,16.4,15.26,15.26,0,0,1-16.79-13.29h0A154,154,0,0,0,340.43,265c-18.64-32.89-47-49.61-84.51-49.61-32.4,0-57.75,9.75-75.19,29-25.14,27.75-30,70.5-26.55,98.78,6.93,48.22,25.46,86.58,58.18,120.86a14.7,14.7,0,0,1-.76,21.11A15.44,15.44,0,0,1,201.31,489.14Z"></path><path d="M372.5,446.18c-32.5,0-60.13-9-82.24-26.89-44.42-35.79-49.4-94.08-49.62-96.54a15.27,15.27,0,0,1,30.45-2.36c.11.86,4.55,48.54,38.79,76,20.26,16.18,47.34,22.6,80.71,18.85a15.2,15.2,0,0,1,16.91,13.18,14.92,14.92,0,0,1-13.44,16.5A187,187,0,0,1,372.5,446.18Z"></path><path d="M398.18,48.79C385.5,40.54,340.54,16,256,16c-88.74,0-133.81,27.11-143.78,34a11.59,11.59,0,0,0-1.84,1.4.36.36,0,0,1-.22.1,14.87,14.87,0,0,0-5.09,11.15A15.06,15.06,0,0,0,120.38,77.5a15.56,15.56,0,0,0,8.88-2.79c.43-.32,39.22-28.82,126.77-28.82S382.58,74.29,383,74.5a15.25,15.25,0,0,0,9.21,3A15.06,15.06,0,0,0,407.5,62.61,14.9,14.9,0,0,0,398.18,48.79Z"></path></g></svg>
                                            <span class="nav-label"></span>
                                        </a>
                                    </li>
                                    {{-- <li class="nav-item">
                                        <a class="nav-link" id="apps-tab" data-toggle="tab" href="#apps" role="tab" aria-controls="apps" aria-selected="false">
                                            <svg fill="#ffffff" width="32px" height="32px" viewBox="-20.08 -20.08 140.56 140.56" id="Layer_1" version="1.1" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" stroke="#ffffff"><g id="SVGRepo_bgCarrier" stroke-width="0"><rect x="-20.08" y="-20.08" width="140.56" height="140.56" rx="70.28" fill="#fe2775" strokewidth="0"></rect></g><g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g><g id="SVGRepo_iconCarrier"> <g> <path d="M79.1,10.1H63.2V3.3c0-0.8-0.7-1.5-1.5-1.5H38c-0.8,0-1.5,0.7-1.5,1.5v6.8H19.7c-3.5,0-6.3,2.8-6.3,6.3v75.4 c0,3.5,2.8,6.3,6.3,6.3h59.5c3.5,0,6.3-2.8,6.3-6.3V16.4C85.4,12.9,82.6,10.1,79.1,10.1z M38,29.5h23.7c0.8,0,1.5-0.7,1.5-1.5v-6.5 h10.5v64.7H25.1V21.6h11.4V28C36.5,28.9,37.2,29.5,38,29.5z M60.2,4.8V20c0,0,0,0,0,0s0,0,0,0v6.5H39.5v-6.5c0,0,0,0,0,0s0,0,0,0 v-8.5c0,0,0,0,0,0s0,0,0,0V4.8L60.2,4.8L60.2,4.8z M82.4,91.7c0,1.8-1.5,3.3-3.3,3.3H19.7c-1.8,0-3.3-1.5-3.3-3.3V16.4 c0-1.8,1.5-3.3,3.3-3.3h16.8v5.5H23.6c-0.8,0-1.5,0.7-1.5,1.5v67.7c0,0.8,0.7,1.5,1.5,1.5h51.6c0.8,0,1.5-0.7,1.5-1.5V20.1 c0-0.8-0.7-1.5-1.5-1.5h-12v-5.5h15.9c1.8,0,3.3,1.5,3.3,3.3V91.7z"></path> <path d="M28.8,46.4c0,0.8,0.7,1.5,1.5,1.5h38.5c0.8,0,1.5-0.7,1.5-1.5s-0.7-1.5-1.5-1.5H30.3C29.5,44.9,28.8,45.6,28.8,46.4z"></path> <path d="M68.8,55.4H30.3c-0.8,0-1.5,0.7-1.5,1.5s0.7,1.5,1.5,1.5h38.5c0.8,0,1.5-0.7,1.5-1.5S69.6,55.4,68.8,55.4z"></path> <path d="M68.8,66.4H30.3c-0.8,0-1.5,0.7-1.5,1.5c0,0.8,0.7,1.5,1.5,1.5h38.5c0.8,0,1.5-0.7,1.5-1.5C70.3,67.1,69.6,66.4,68.8,66.4z "></path> </g> </g></svg>
                                            <span class="nav-label"></span>
                                        </a>
                                    </li> --}}
                                    <li class="nav-item">
                                        <a class="nav-link" id="pages-tab" data-toggle="tab" href="#pages" role="tab" aria-controls="pages" aria-selected="false">
                                            

                                            <svg fill="#ffffff" width="32px" height="32px" viewBox="-307.2 -307.2 1638.40 1638.40" xmlns="http://www.w3.org/2000/svg" stroke="#ffffff"><g id="SVGRepo_bgCarrier" stroke-width="0"><rect x="-307.2" y="-307.2" width="1638.40" height="1638.40" rx="819.2" fill="#fe2775" strokewidth="0"></rect></g><g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g><g id="SVGRepo_iconCarrier"><path d="M960 95.888l-256.224.001V32.113c0-17.68-14.32-32-32-32s-32 14.32-32 32v63.76h-256v-63.76c0-17.68-14.32-32-32-32s-32 14.32-32 32v63.76H64c-35.344 0-64 28.656-64 64v800c0 35.343 28.656 64 64 64h896c35.344 0 64-28.657 64-64v-800c0-35.329-28.656-63.985-64-63.985zm0 863.985H64v-800h255.776v32.24c0 17.679 14.32 32 32 32s32-14.321 32-32v-32.224h256v32.24c0 17.68 14.32 32 32 32s32-14.32 32-32v-32.24H960v799.984zM736 511.888h64c17.664 0 32-14.336 32-32v-64c0-17.664-14.336-32-32-32h-64c-17.664 0-32 14.336-32 32v64c0 17.664 14.336 32 32 32zm0 255.984h64c17.664 0 32-14.32 32-32v-64c0-17.664-14.336-32-32-32h-64c-17.664 0-32 14.336-32 32v64c0 17.696 14.336 32 32 32zm-192-128h-64c-17.664 0-32 14.336-32 32v64c0 17.68 14.336 32 32 32h64c17.664 0 32-14.32 32-32v-64c0-17.648-14.336-32-32-32zm0-255.984h-64c-17.664 0-32 14.336-32 32v64c0 17.664 14.336 32 32 32h64c17.664 0 32-14.336 32-32v-64c0-17.68-14.336-32-32-32zm-256 0h-64c-17.664 0-32 14.336-32 32v64c0 17.664 14.336 32 32 32h64c17.664 0 32-14.336 32-32v-64c0-17.68-14.336-32-32-32zm0 255.984h-64c-17.664 0-32 14.336-32 32v64c0 17.68 14.336 32 32 32h64c17.664 0 32-14.32 32-32v-64c0-17.648-14.336-32-32-32z"></path></g></svg>
                                            <span class="nav-label"></span>
                                        </a>
                                    </li>
                                </ul>
                    
                                <!-- separator -->
                                <div class="separator-large"></div>
                                <!-- end separator -->
                    
                                <div class="tab-content">
                                    <div class="tab-pane fade active show" id="features" role="tabpanel" aria-labelledby="features-tab">
                                        <div class="container">
                                        </div>
                    
                                        <!-- separator -->
                                        {{-- <div class="separator-medium"></div> --}}
                                        <!-- end separator -->
                    
                                        <div class="list-view list-colored">
                                            <ul>
                                                @if(isset($data))

                                                @foreach ($data as $dt)
                                                <li class="list-item">
                                                    <div class="list-media">
                                                        {{-- <img src="{{asset('mobile_theme/images/icon-time.png')}}" alt="Presensi"></a> --}}
                                                        <svg fill="#ffffff" width="32px" height="32px" viewBox="-51.2 -51.2 614.40 614.40" xmlns="http://www.w3.org/2000/svg" stroke="#ffffff"><g id="SVGRepo_bgCarrier" stroke-width="0"><rect x="-51.2" y="-51.2" width="614.40" height="614.40" rx="307.2" fill="#fe2775" strokewidth="0"></rect></g><g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g><g id="SVGRepo_iconCarrier"><title>ionicons-v5-i</title><path d="M63.28,202a15.29,15.29,0,0,1-7.7-2,14.84,14.84,0,0,1-5.52-20.46C69.34,147.36,128,72.25,256,72.25c55.47,0,104.12,14.57,144.53,43.29,33.26,23.57,51.9,50.25,60.78,63.1a14.79,14.79,0,0,1-4,20.79,15.52,15.52,0,0,1-21.24-4C420,172.32,371,102,256,102c-112.25,0-163,64.71-179.53,92.46A15,15,0,0,1,63.28,202Z"></path><path d="M320.49,496a15.31,15.31,0,0,1-3.79-.43c-92.85-23-127.52-115.82-128.93-119.68l-.22-.85c-.76-2.68-19.39-66.33,9.21-103.61,13.11-17,33.05-25.72,59.38-25.72,24.48,0,42.14,7.61,54.28,23.36,10,12.86,14,28.72,17.87,44,8.13,31.82,14,48.53,47.79,50.25,14.84.75,24.59-7.93,30.12-15.32,14.95-20.15,17.55-53,6.28-82C398,228.57,346.61,158,256,158c-38.68,0-74.22,12.43-102.72,35.79C129.69,213.14,111,240.46,102,268.54c-16.69,52.28,5.2,134.46,5.41,135.21A14.83,14.83,0,0,1,96.54,422a15.39,15.39,0,0,1-18.74-10.6c-1-3.75-24.38-91.4-5.1-151.82,21-65.47,85.81-131.47,183.33-131.47,45.07,0,87.65,15.32,123.19,44.25,27.52,22.5,50,52.72,61.76,82.93,14.95,38.57,10.94,81.86-10.19,110.14-14.08,18.86-34.13,28.72-56.34,27.65-57.86-2.9-68.26-43.29-75.84-72.75-7.8-30.22-12.79-44.79-42.58-44.79-16.36,0-27.85,4.5-35,13.82-9.75,12.75-10.51,32.68-9.43,47.14a152.44,152.44,0,0,0,5.1,29.79c2.38,6,33.37,82,107.59,100.39a14.88,14.88,0,0,1,11,18.11A15.36,15.36,0,0,1,320.49,496Z"></path><path d="M201.31,489.14a15.5,15.5,0,0,1-11.16-4.71c-37.16-39-58.18-82.61-66.09-137.14V347c-4.44-36.1,2.06-87.21,33.91-122.35,23.51-25.93,56.56-39.11,98.06-39.11,49.08,0,87.65,22.82,111.7,65.89,17.45,31.29,20.91,62.47,21,63.75a15.07,15.07,0,0,1-13.65,16.4,15.26,15.26,0,0,1-16.79-13.29h0A154,154,0,0,0,340.43,265c-18.64-32.89-47-49.61-84.51-49.61-32.4,0-57.75,9.75-75.19,29-25.14,27.75-30,70.5-26.55,98.78,6.93,48.22,25.46,86.58,58.18,120.86a14.7,14.7,0,0,1-.76,21.11A15.44,15.44,0,0,1,201.31,489.14Z"></path><path d="M372.5,446.18c-32.5,0-60.13-9-82.24-26.89-44.42-35.79-49.4-94.08-49.62-96.54a15.27,15.27,0,0,1,30.45-2.36c.11.86,4.55,48.54,38.79,76,20.26,16.18,47.34,22.6,80.71,18.85a15.2,15.2,0,0,1,16.91,13.18,14.92,14.92,0,0,1-13.44,16.5A187,187,0,0,1,372.5,446.18Z"></path><path d="M398.18,48.79C385.5,40.54,340.54,16,256,16c-88.74,0-133.81,27.11-143.78,34a11.59,11.59,0,0,0-1.84,1.4.36.36,0,0,1-.22.1,14.87,14.87,0,0,0-5.09,11.15A15.06,15.06,0,0,0,120.38,77.5a15.56,15.56,0,0,0,8.88-2.79c.43-.32,39.22-28.82,126.77-28.82S382.58,74.29,383,74.5a15.25,15.25,0,0,0,9.21,3A15.06,15.06,0,0,0,407.5,62.61,14.9,14.9,0,0,0,398.18,48.79Z"></path></g></svg>
                                                        
                                                        {{-- <i class="icon ion-ios-arrow-down bg-blue"></i> --}}
                                                    </div>
                                                    <div class="list-label">
                                                        {{-- {{@$dt->PresenseEmployee->employee_name}} --}}
                                                        <div class="list-title">
                                                             {{\Carbon\Carbon::parse(@$dt->date_log)->format('d-M-Y G:i')}}
                                                        </div>
                                                        {{-- <div class="list-after"><i class="icon ion-ios-arrow-forward"></i></div> --}}
                                                    </div>
                                                </li>
                                                @endforeach
                                                @endif
                                                
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="apps" role="tabpanel" aria-labelledby="apps-tab">
                    
                                        <!-- separator -->
                                        <div class="separator-medium"></div>
                                        <!-- end separator --> --}}
                    
                                        <div class="list-view list-colored">
                                            <ul>
                                                <li class="list-item">
                                                    <div class="list-media">
                                                        <i class="icon ion-ios-chatboxes bg-red"></i>
                                                    </div>
                                                    <div class="list-label">
                                                        <div class="list-title">Chat</div>
                                                        <div class="list-after"><i class="icon ion-ios-arrow-forward"></i></div>
                                                    </div>
                                                </li>
                                                
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="pages" role="tabpanel" aria-labelledby="pages-tab">
                                        
                                        
                                        <div class="list-view list-colored">
                                            <ul>

                                                @isset($datacuti->get_cutiemps)

                                                    @foreach ($datacuti->get_cutiemps as $item)
                                                    <li class="list-item">
                                                        <div class="list-media">
                                                            <i class="icon ion-ios-information-circle bg-info"></i>
                                                        </div>
                                                        <div class="list-label">
                                                            <div class="list-title"><b>{{$item->get_cuti_categories->jenis_cuti}} </b>/ {{$item->tgl_awal}} s.d. {{$item->tgl_akhir}} <span class="badge badge-info">{{$item->jumlah_hari}} hari</span></div>
                                                            {{-- icon detail --}}
                                                            {{-- <div class="list-after"><i class="icon ion-ios-arrow-forward"></i></div> --}}
                                                        </div>
                                                    </li>
    
                                                        
                                                    @endforeach
                                                @endisset 
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                    
                            </div>

                            {{-- end tab --}}

                        </div>
                    </div>
                </div>
            </div>
        </div>


        {{-- end list history --}}

		<!-- feature slide -->
            {{-- <div class="feature-slide">
                <div class="swiper-container swiper-style">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="content shadow-sm">
                                <div class="info-statistic bg-blue">10+</div>
                                <div class="icon"><i class="icon ion-ios-star color-blue"></i></div>
                                <div class="text">
                                    <h5>Features</h5>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="content shadow-sm">
                                <div class="info-statistic bg-red">9+</div>
                                <div class="icon"><i class="icon ion-ios-apps color-red"></i></div>
                                <div class="text">
                                    <h5>Apps</h5>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="content shadow-sm">
                                <div class="info-statistic bg-yellow">12+</div>
                                <div class="icon"><i class="icon ion-ios-browsers color-yellow"></i></div>
                                <div class="text">
                                    <h5>Pages</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> --}}

		<!-- feature list -->
		{{-- <div class="feature-list">
			<div class="swiper-container swiper-style-full">
				<div class="swiper-wrapper">
					<div class="swiper-slide">
						<div class="content bg-lightblue">
							<div class="wrap-icon">
								<i class="icon ion-ios-brush bg-blue"></i>
							</div>
							<div class="text">
								<h5>Easy to Edit</h5>
								<p>With a neat page layout and well arranged code</p>
							</div>
						</div>
					</div>
					<div class="swiper-slide">
						<div class="content bg-lightgreen">
							<div class="wrap-icon">
								<i class="icon ion-ios-checkbox bg-green"></i>
							</div>
							<div class="text">
								<h5>Complete Features</h5>
								<p>Equipped with various features such as lists, buttons, and others</p>
							</div>
						</div>
					</div>
					<div class="swiper-slide">
						<div class="content bg-lightred">
							<div class="wrap-icon">
								<i class="icon ion-ios-keypad bg-red"></i>
							</div>
							<div class="text">
								<h5>Well Documented</h5>
								<p>So that users easily use and customize this template</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div> --}}
		<!-- feature list -->

		<!-- separator -->
		{{-- <div class="separator-large"></div> --}}
		<!-- end separator -->

		<!-- newsletter -->
		{{-- <div class="newsletter bg-lightblue"> --}}

			<!-- separator -->
			{{-- <div class="separator-medium"></div> --}}
			<!-- end separator -->

			{{-- <div class="section-title text-center title-large">
				<h3>Subscribe Newsletter</h3>
				<p class="text-small mt-1">Enter you email to recieive newsletters</p>
			</div>
			<div class="container">

				<form class="form-fill form-icon-inset">
					<div class="form-wrapper">
						<div class="input-wrap bg-white">
							<input type="email" placeholder="Email" class="bg-white">
							<a href="#">
								<div class="button-icon">
									<i class="icon ion-ios-checkmark"></i>
								</div>
							</a>
						</div>
					</div>
				</form>
			</div> --}}

			<!-- separator -->
			{{-- <div class="separator-large"></div> --}}
			<!-- end separator -->

		{{-- </div> --}}
		<!-- end newsletter -->

		<!-- separator -->
		{{-- <div class="separator-small"></div> --}}
		<!-- end separator -->

		<!-- footer -->
		{{-- <div class="footer">
			<div class="container">
				<div class="content-box shadow-sm">
					<h4 class="mb-1">Maxui</h4>
					<span>Mobile Template</span>

					<!-- separator -->
					<div class="separator-small"></div>
					<!-- end separator -->

					<div class="social-media-icon">
						<ul>
							<li><a href="#"><i class="icon ion-logo-facebook bg-facebook"></i></a></li>
							<li><a href="#"><i class="icon ion-logo-instagram bg-instagram"></i></a></li>
							<li><a href="#"><i class="icon ion-logo-twitter bg-twitter"></i></a></li>
							<li><a href="#"><i class="icon ion-logo-whatsapp bg-whatsapp"></i></a></li>
						</ul>
					</div>

					<!-- separator -->
					<div class="separator-small"></div>
					<!-- end separator -->

					<p class="made-by text-small">Made With <i class="icon ion-ios-heart"></i> aStylers</p>
				</div>
			</div>
		</div> --}}
		<!-- end footer -->

		<!-- separator -->
		{{-- <div class="separator-large"></div> --}}
		<!-- end separator -->

	</div>
	<!-- end page wrapper -->

	<!-- toolbar bottom -->
	@include('layout_mobile.menubawa')
	<!-- end toolbar bottom -->

	@include('layout_mobile.js')



</body>
</html>