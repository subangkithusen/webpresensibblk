<!DOCTYPE html>
<html lang="zxx">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">

	<link rel="icon" href="images/favicon.png">
	<title>Maxui</title>

	<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900&display=swap" rel="stylesheet">

    

	<link rel="stylesheet" href="{{asset('mobile_theme/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{asset('mobile_theme/css/ionicons.min.css')}}">
	<link rel="stylesheet" href="{{asset('mobile_theme/css/fakeLoader.css')}}">
	<link rel="stylesheet" href="{{asset('mobile_theme/css/swiper.min.css')}}">
	<link rel="stylesheet" href="{{asset('mobile_theme/css/style.css')}}">

</head>
<body>
	
	<!-- fakeloader -->
	<div class="fakeLoader"></div>
	<!-- end fakeloader -->
	
	<!-- navbar -->
	<div class="navbar">
		<div class="left">
			<a href="" class="link link-back"><i class="icon ion-ios-arrow-back"></i></a>
		</div>
		<div class="title">
			Sign In
		</div>
		<div class="right">
			
		</div>
	</div>
	<!-- end navbar -->

	<!-- pages wrapper -->
	<div class="pages-wrapper">

		<!-- separator -->
		<div class="separator-large"></div>
		<!-- end separator -->

        {{-- container --}}
        {{-- <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            
                        </div>
                    </div>
                </div>
                
            </div>
        </div> --}}

        {{-- endcontainer --}}
		
		<!-- sign in -->
        <form method="POST" action="{{ route('login') }}">
            @csrf
		<div class="sign in">
			<div class="container">
				<form class="form-fill">
					<div class="form-wrapper">
						<div class="input-wrap">
							<input type="text" name="username" placeholder="NIK">
						</div>
						<div class="input-wrap">
							<input type="password" name="password" placeholder="Password">
						</div>
					</div>
					<div class="button-default">
						<button class="button">Sign In</button>
					</div>
				</form>

				<!-- separator -->
				<div class="separator-medium"></div>
				<!-- end separator -->

				{{-- <div class="link-forgot text-center text-small">
					<a href="reset-password.html" class="color-theme">Forgot Password?</a>
				</div> --}}

				<!-- separator -->
				<div class="separator-small"></div>
				<!-- end separator -->

				{{-- <div class="link-sign-up text-center text-small">
					<p>Don't have an account?<a href="sign-up.html" class="color-theme ml-2">Sign Up</a></p>
				</div> --}}

				<!-- separator -->
				<div class="separator-large"></div>
				<!-- end separator -->

				{{-- <div class="sign-with">
					<ul>
						<li>
							<a href="#"><i class="icon ion-logo-facebook mr-2"></i>Facebook</a>
						</li>
						<li>
							<a href="#"><i class="icon ion-logo-google mr-2"></i>Google</a>
						</li>
					</ul>
				</div> --}}
			</div>
		</div>
        </form>
		<!-- end sign in -->

	</div>
	<!-- end pages wrapper -->

	<script src="{{asset('mobile_theme/js/jquery-3.4.1.min.js')}}"></script>
	<script src="{{asset('mobile_theme/js/bootstrap.bundle.min.js')}}"></script>
	<script src="{{asset('mobile_theme/js/fakeLoader.js')}}"></script>
	<script src="{{asset('mobile_theme/js/swiper.min.js')}}"></script>
	<script src="{{asset('mobile_theme/js/main.js')}}"></script>

</body>
</html>