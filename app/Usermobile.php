<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

// use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Usermobile extends Model
{
    //
    use HasFactory, Notifiable;
    protected $fillable = [
        'name',
        'employee_number',
        'email',
        'password',
        'rememberToken',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];



    public function isUsermobile()
    {
        return $this->role === 'usermobile';
    }
}
