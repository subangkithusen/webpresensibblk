<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cutiempsmodel extends Model
{
    protected $table = 'emp_cuties';//nama tabel
    protected $primaryKey = 'id';
    // employee_id,cuti,tahun,created_at,updated_at

    public function get_emp_kategori(){
        return $this->hasOne(Cutiketegorimodel::class,'id','cuti_categories_id');
    }
    
   
}
