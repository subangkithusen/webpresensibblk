<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PresensiModel extends Model
{
    protected $table = 'presence_records';//nama tabel
    protected $primaryKey = 'id';
    protected $fillable = ['id','employee_number','date_log','status','terminal_id','finger_print_id','created_at','updated_at'];//isi tabel
    // public $timestamps = false;

    public function PresenseEmployee(){
        return $this->belongsTo(EmployeeModel::class,'employee_number','employee_number');
    }


}
