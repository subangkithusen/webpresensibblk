<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usergambarmodel extends Model
{
    
    protected $table = 'login_image_user';//nama tabel
    protected $primaryKey = 'id';
    protected $fillable = ['username_id','gambar_pegawai','created_at','updated_at'];//isi tabel
    // public $timestamps = false;

    public function Presence_record(){
        return $this->belongsTo(EmployeeModel::class,'employee_number','username_id');
    }

    public function user(){
        return $this->hasOne(User::class.'id','id');
    }
}
