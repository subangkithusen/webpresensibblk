<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PresensiModel as mp;
use App\Usergambarmodel as usergambar;
use App\Empcutimodel as cm;
use App\EmployeeModel as pegawai ;
use Carbon\carbon as carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use DB;
use App\Cutiemp as CT;
class PresensiController extends Controller
{
    public function __Construct(){
        $this->middleware('auth');
    }
    function index(){

        dd($this->findEmployeeCuti());
        //get auth disini
        $userdata = auth()->user();
        $id = $userdata->username;
        // dd($this->get_cuti('1'));

        // $iddummy = '197801122005011001';
        $datacuti = pegawai::where('employee_number','=',$id)->first();
        // dd($datacuti->get_cutiemps);
        foreach($datacuti->get_cutiemps as $cuti){
            // dd($cuti);
            // dd($cuti->get_cuti_categories->jenis_cuti);
            // dd($cuti->get_cutiemps);
        //    dd($cuti->get_cutiemps[0]->get_cuti_categories); 
        }       
        if($id !== null){
            $data = mp::where('employee_number','=',$id)->orderBy('date_log','DESC')->take(10)->get();
            $datain = mp::where('employee_number','=',$id)->whereDate('date_log',carbon::today())->orderBy('date_log','asc')->take(1)->first();
            $dataout = mp::where('employee_number','=',$id)->whereDate('date_log',carbon::today())->orderBy('date_log','desc')->take(1)->first();
        }else{
            dd("id null");
        }
        return view('mobile',compact('data','datain','dataout','userdata','datacuti'));
    }
    public function create(){
        $userdata = auth()->user();
        $id = $userdata->username;
        $historyPresensi = usergambar::where('username_id','=',$id)->whereDate('created_at',carbon::today())->orderBy('id','DESC')->get();
        return view('pages_mobile.presensi.index',compact('historyPresensi'));
    }

    function validasi(){
        dd("cek validasi ip");
    }
    function hari(){

    }
    Function terminal_id($data){
        return $data;
    }
    public function max_applogin($nik){
        $datacount = usergambar::where('username_id','=',$nik)->whereDate('created_at',carbon::today())->count();
        return $datacount;
    }

    public function store(Request $request){
        // dd($request->mydata);
        // dd("sabar masih on going");
        $ip_localhost = '127.0.0.1';
        $ip_public = '202.180.26.222';
        $decode_gambar = base64_decode($request->mydata);
        //create
        // $datasave = usergambar::create([
        //     'username_id' => '199107092023211015',
        //     'gambar_pegawai' =>$decode_gambar,
        //     'created_at' =>  Carbon::now(),
        //     'updated_at' => Carbon::now()
        // ]);
        // dd($datasave);
        $nik = auth()->user()->username;
        // $validasicountall = mp::where('employee_number','=',$nik)->whereDate('date_log',carbon::today())->orderBy('date_log','desc')->count();
        // // dd($validasicountall); // -> 0;
        if($this->get_client_ip() !== $ip_public){
            // dd("gagal insert");
            return view('pages_mobile.presensi.gagalinsert');
            //return ke halaan konek wifi
        }else{
            // jika benar lakukan disini  -> validasi sudah melakukan finger max 4x
            if($this->max_applogin($nik) > 2){
            //    $countanda 
                return view('pages_mobile.presensi.infocount');
            }else{
                $datasave = usergambar::create([
                    'username_id' => $nik,
                    'gambar_pegawai' =>$decode_gambar,
                    'created_at' =>  Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);
                $data_now = carbon::now()->format('Y-m-d H:i:s');
                // ambil manual dulu karena data harus diregis ke mesin absensi
                $finger_print_id = $data = mp::where('employee_number','=',auth()->user()->username)->orderBy('date_log','DESC')->first();
                //masukkan datalog
                $data = mp::create([
                    'employee_number' => auth()->user()->username,
                    'date_log' => $data_now,
                    'status' =>'0',
                    'terminal_id' =>'1',
                    'finger_print_id' =>$finger_print_id->finger_print_id,
                ]);
                return redirect()->route('halaman-utama.index');
            }
        }
        dd("test");
        /*
        blob to image open
        $rdm = Str::random(10);
        $uploads_dir = "uploads/";
        $encoded_data = $request->mydata;
	    $binary_data = base64_decode( $encoded_data );
	    $result = file_put_contents($uploads_dir.auth()->user()->username.'-'.$rdm.'.jpg', $binary_data );
        end blob
        */
        $ip_public = '202.180.26.222';
        $data_now = carbon::now()->format('Y-m-d H:i:s');
        // ambil manual dulu karena data harus diregis ke mesin absensi
        $finger_print_id = $data = mp::where('employee_number','=',auth()->user()->username)->orderBy('date_log','DESC')->first();
        /*
        if($data){
            return redirect()->route('barang.index')->with(['success' => 'Data Berhasil Disimpan']);
        //  return redirect()->route('posts.index')->with(['success' => 'Data Berhasil Disimpan!']);
        }else{
            return redirect()->route('barang.index')->with(['error' => 'Data Gagal Disimpan']);
        }
        */
    }
    function getPegawai($id){ //ambil perpegawai
        return $id;
    }
    function get_client_ip() {//function get ip
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }
    public function get_cuti($id){
        //get user relasi ke employee
        return $id;
    }


    // function get kains or kasubag

    public function findEmployeeCuti()
    {
        // $find = Request::get('term');
        $find = '199107092023211015';
        $results = array();
        $sql = pegawai::
            select('id',DB::raw('CONCAT(employee_number," - ", employee_name) AS emp'), 'employee_name', 'employee_number')
            ->where('employee_name', 'LIKE', '%'.$find.'%')
            ->orWhere('employee_number', 'LIKE', $find.'%')
            ->take(15)->get();
            
        foreach ($sql as $val)
        {
            $cek        = DB::table('employees')->where('employee_number','=', $val->employee_number)->first();
            $work       = DB::table('emp_work_units')->where('id', '=', $cek->emp_work_unit_id)->first(); //cek employee number id
            $pimpinan   = DB::table('emp_work_units')->where('id', '=', $work->pimpinan_id)->first();

            /* -- work --
            +"id": 16
            +"work_unit_desc": "Sub Bagian Administrasi Umum"
            +"status": 1
            +"pimpinan_id": 27
            +"unit_category": "non pelayanan"
            +"iku": "1.00"
            +"created_at": "2018-05-09 01:54:42"
            +"updated_at": "2021-08-03 15:11:31"
            */ 
            
            /* -- pimpinan --- ;
              +"id": 27
            +"work_unit_desc": "Kepala Sub Bagian Administrasi Umum"
            +"status": 1
            +"pimpinan_id": 2
            +"unit_category": "non pelayanan"
            +"iku": "1.00"
            +"created_at": "2018-11-07 02:39:06"
            +"updated_at": "2021-02-05 15:32:55"
            */

            if($pimpinan == null){
                $atasan = (object) [
                    'employee_name' => 'BBLK'
                ];
            }else{
                $atasan     = DB::table('employees')->where('emp_work_unit_id', '=', $pimpinan->id)->first();
                // dd($atasan);
                if(empty($atasan)){
                    $atasan = 'BBLK';
                }else{
                    $atasan     = DB::table('employees')->where('emp_work_unit_id', '=', $pimpinan->id)->first();
                }
            }

            $cuti       = DB::table('emp_cuties')->where('employee_id', '=', $val->id)->where('tahun', '=', date('Y'))->first();
            $sqlCuti = CT::select(
                DB::raw('sum(jumlah_hari) as cuti'))
                ->where('employee_number', '=',$val->employee_number)
                ->where('cuti_categories_id', '=', 1)
                ->where(DB::raw("DATE_FORMAT(tgl_awal, '%Y')"), '=',date('Y'))
                ->first();
            $tahuncuti = $cuti->cuti;
            if($tahuncuti == null){
                $tahuncuti = 0;
            }
            $saldo = $tahuncuti-$sqlCuti->cuti;
            if(!empty($atasan)){
                if($atasan == 'BBLK'){
                    $results[] = [ 'id' => $val->id, 'value' => $val->emp, 'emp_name' => $val->employee_name, 'emp_number' => $val->employee_number, 'cuti' => $saldo, 'atasan' => 'BBLK'];    
                }else{
                    $results[] = 
                    [ 
                        'id' => $val->id,
                        'value' => $val->emp,
                        'emp_name' => $val->employee_name,
                        'emp_number' => $val->employee_number,
                        'cuti' => $saldo,
                        'atasan' => $atasan->employee_name,
                        'id_atasan' => $atasan->id
                    ];
                }
                
            }else{
                $results[] = [ 'id' => $val->id, 'value' => $val->emp, 'emp_name' => $val->employee_name, 'emp_number' => $val->employee_number, 'cuti' => $saldo];
            }
        }
        // dd($results);

        return $results;
        // return response()->json($results);
    }



}
