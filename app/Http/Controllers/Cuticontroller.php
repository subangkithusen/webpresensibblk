<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cutikategorimodel as cm;
use App\EmployeeModel as Employee;
use App\Cutiemp as ct;
use Carbon\Carbon;
use DB;
class Cuticontroller extends Controller
{
    
    public function index(){
        $c_cuti = cm::orderBy('id','ASC')->get();

        $now = Carbon::now();
        // open
        $startDate = Carbon::parse("2024-01-01");
        $endDate = Carbon::parse("2024-01-22");
        $days = $startDate->diffInDaysFiltered(function (Carbon $date){
            return !$date->isWeekday();
        }, $endDate);
        // dd($days);

        /* open */
        $find = '197801122005011001';
        $results = array();
        $sql = Employee::
            select('id',DB::raw('CONCAT(employee_number," - ", employee_name) AS emp'), 'employee_name', 'employee_number', 'emp_job_title_id', 'emp_work_unit_id')
            ->where('employee_name', 'LIKE', '%'.$find.'%')
            ->orWhere('employee_number', 'LIKE', $find.'%')
            ->take(15)->get();

        foreach ($sql as $val)
        {
            $results[] = [ 'id' => $val->id,
			'value' => $val->emp,
			'emp_name' => $val->employee_name,
			'emp_number' => $val->employee_number,
			'emp_work_unit_id' => $val->emp_work_unit_id,
			'emp_job_title_id' => $val->emp_job_title_id];
        }
        dd($results);

        /* close */





        // close
        //This weekend's datarange
        $starOftWeekend = $now->endOfWeek()->subDays(2)->format('Y-m-d H:i'); //+2
        $endOfWeekend   = $now->endOfWeek()->format('Y-m-d H:i');
        //Weekly date range
        $startOfWeek = $now->startOfWeek()->format('Y-m-d H:i'); 
        $endOfWeek = $now->endOfWeek()->format('Y-m-d H:i'); 

        // dd($startOfWeek);

        return view('pages_mobile.cuti.index',compact('c_cuti'));
    }
    public function create(){
        //kirim id cutinya
        return view('pages_mobile.cuti.cuti');
    }
    public function createid(Request $request){
        dd($request->all());
    }
}
