<?php

namespace App\Http\Middleware;

use Closure;

class UsermobileMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }
    // public function handle($request, Closure $next)
    // {
    //     if (!auth()->guard('web')->check() || !auth()->guard('web')->user()->isUsermobile()) {
    //         return redirect('/')->with('error', 'Unauthorized access.');
    //     }

    //     return $next($request);
    // }

    
}
