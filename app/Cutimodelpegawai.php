<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cutimodelpegawai extends Model
{
    protected $table = 'cuti_emps';//nama tabel
    protected $primaryKey = 'id';
    // protected $fillable = ['employee_id','employee_number',''];//isi tabel
    // public $timestamps = false;

    //cuti_kategories
    public function get_cuti_categories(){
            return $this->hasOne(Cutikategorimodel::class,'id','cuti_categories_id');
    }

}
