<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeModel extends Model
{
    protected $table = 'employees';//nama tabel
    protected $primaryKey = 'id';
    
    //cuti_emps -> Cutimodelpegawai
    public function get_cutiemps(){
        return $this->hasMany(Cutimodelpegawai::class,'employee_id','id');
    }

    //emp_cuties -> totalcutisaat ini
    public function get_emp_cuties(){
        return $this->hasMany(Cutiempsmodel::class,'employee_id','id')->orderBy('tahun','DESC');
    }

    //emp_work_units
    public function get_emp_work_units(){
        return $this->hasOne(Workunitmodel::class,'id','emp_work_unit_id');
    }

    /*
    public function userSorted {
    $this->user->orderBy('companies_title');
    }
    */ 
    
}
